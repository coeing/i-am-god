# **Design stuffz** #

## **Elemental properties:** ##
### **Fire-** ###
Fire can be placed by pointing at the desired location and pressing the “A”-button. Holding the button will increase the intensity from a spark to a flash fire to an inferno to a giant pillar of fire.
Fire will set on fire and eventually kill anything living that comes into contact with it. It can also spread around this way.
Fire will not spread if it has nothing to move onto. It will also eventually die out on its own if it can't be sustained.
Fire will be removed if it comes into contact with water. Burning victims can be saved this way.
Fire will only move in the direction of a gust or harder wind.
### **Air-** ###
Wind is controlled by pressing the “A”-button, and then moving the controller in the desired direction. The wind will blow in the same direction as was pointed. Holding the “A” button will increase intensity from a slight breeze, to a gust, to a gale, to a storm, to a hurricane.
All wind is affected by previous wind. If a hurricane is blowing South, a hurricane to the north will only cancel out the original hurricane.
### **Earth-** ###
Earth is controlled by pressing the “A”-button, and then moving he controller up (for mountains), or down (for valleys). Moving the controller faster will increase the height or depth. Holding the button will cause a volcano for mountains(basically, fire will start spreading from it) or a fissure in the case of a valley(a long, uncrossable chasm).
Water will pool in a valley, allowing it to stay after the rain has stopped.
Wind is blocked from affecting the lee of a mountain. Wind has no effect on anyone in a valley. Anyone on a mountain is affected stronger by wind.
###** Water- **###
Rain can be placed by pointing at the desired location and pressing the “A”-button. Holding the button will increase the intensity from a light drizzle, to a shower, to regular rain to intense rain, to a monsoon.
Water dissipates after a while, or when it comes into contact with fire, unless the water is in a valley.
The Prophet moves as normal on water. Followers are slowed down.


## **Puzzles** ##
###** Start **###
### Scenario-  ###
The prophet arrives at a village and must convince the population of authenticity.
### Solution- ###
Use every power at least once.
The Prophet must survive.
At least one follower must survive.
### Extra: ###
Dialog boxes show up, telling you how to do each action. The boxes follow the order of air, earth, water, fire, but the actions can be done in any order.



### **Hurricane-** ###
###Scenario-###
The Prophet and his followers are hindered by a hurricane blowing straight against them.
### Solution-###
Use wind to cancel out the hurricane (this teaches the player how wind priority works), or build mountain passes or valleys to block the hurricane winds.
The Prophet must survive.
At least one follower must survive.

###**Rough terrain**###
### Scenario-###
The Prophet and his followers reach an area of giant chasms and giant mountains that they cannot cross on their own.
###Solution-###
Use earth to bridge chasms and lower mountains. (Water to let followers swim past chasms, or strong wind to blow them over. Mountains can only really be handled by lowering them. Kind of dull, shouldn't be main obstructions.)
The Prophet must survive.
At least one follower must survive.



### **Brush fire-** ###
###Scenario-###
The Prophet and his followers are walking through a prairie, when the grass suddenly catches fire.
### Solution-###
Use wind to steer the fire away from the V.I.P:s, protect them with strategic rainclouds, use your own fire to create a safe zone where there is no fuel, and/or create mountains to section off the flames.
The Prophet must survive.
At least one follower must survive.

### **Mutant Beavers** ###
###Scenario-###
Mutant beavers are attacking The Prophet and his followers. 
###Solution-###
Get The Prophet past the beavers.
The Prophet must survive.
At least one follower must survive.
### Extra: ###
Beavers always move towards the nearest human.
A human that comes in contact with a beaver will die, but the beaver is detained while finishing it off.
Beavers take a bit of time to die by burning, ignore water completely, can be blocked by mountains, and can be blown around by wind.