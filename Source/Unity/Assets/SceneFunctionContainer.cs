﻿using UnityEngine;
using System.Collections;

public class SceneFunctionContainer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeScene (string nameScene) {
		Application.LoadLevel(nameScene);
	}

	public void QuitGame () {
		Application.Quit();
	}
}
