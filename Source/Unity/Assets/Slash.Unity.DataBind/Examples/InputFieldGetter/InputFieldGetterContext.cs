﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputFieldGetterContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Examples.InputFieldGetter
{
    using Slash.Unity.DataBind.Core.Data;

    using UnityEngine;

    public class InputFieldGetterContext : Context
    {
        #region Fields

        private readonly Collection<string> messages = new Collection<string>();

        /// <summary>
        ///   Data binding property, used to get informed when a data change happened.
        /// </summary>
        private readonly Property<string> textProperty = new Property<string>();

        #endregion

        #region Properties

        public Collection<string> Messages
        {
            get
            {
                return this.messages;
            }
        }

        public string Text
        {
            get
            {
                return this.textProperty.Value;
            }
            set
            {
                this.textProperty.Value = value;
            }
        }

        public Property<string> TextProperty
        {
            get
            {
                return this.textProperty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void OnSendMessage(string message)
        {
            Debug.Log("Message: " + message);
            this.messages.Add(message);

            // Clear current text.
            this.Text = string.Empty;
        }

        #endregion
    }
}