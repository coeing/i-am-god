﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollectionExampleContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Examples.Collection
{
    using System.Linq;

    using Slash.Unity.DataBind.Core.Data;

    public class CollectionExampleContext : Context
    {
        #region Fields

        private readonly Collection<CollectionExampleItemContext> items = new Collection<CollectionExampleItemContext>();

        #endregion

        #region Constructors and Destructors

        public CollectionExampleContext()
        {
            this.items.Add(new CollectionExampleItemContext() { Text = "This" });
            this.items.Add(new CollectionExampleItemContext() { Text = "Is" });
            this.items.Add(new CollectionExampleItemContext() { Text = "Data Bind" });
        }

        #endregion

        #region Public Properties

        public Collection<CollectionExampleItemContext> Items
        {
            get
            {
                return this.items;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void OnAddItem()
        {
            this.items.Add(new CollectionExampleItemContext() { Text = this.items.Count.ToString() });
        }

        public void OnRemoveItem()
        {
            if (this.items.Count > 0)
            {
                this.items.Remove(this.items.Last<CollectionExampleItemContext>());
            }
        }

        #endregion
    }
}