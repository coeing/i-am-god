// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollectionExampleItemContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Examples.Collection
{
    using Slash.Unity.DataBind.Core.Data;

    public class CollectionExampleItemContext : Context
    {
        #region Fields

        private readonly Property<string> textProperty = new Property<string>();

        #endregion

        #region Public Properties

        public string Text
        {
            get
            {
                return this.textProperty.Value;
            }
            set
            {
                this.textProperty.Value = value;
            }
        }

        public Property<string> TextProperty
        {
            get
            {
                return this.textProperty;
            }
        }

        #endregion
    }
}