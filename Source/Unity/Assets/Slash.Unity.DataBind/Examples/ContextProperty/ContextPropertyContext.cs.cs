﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextPropertyContext.cs.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Examples.ContextProperty
{
    using Slash.Unity.DataBind.Core.Data;

    using UnityEngine;

    public class ContextPropertyContext : Context
    {
        #region Fields

        private readonly Collection<ContextPropertyItemContext> items = new Collection<ContextPropertyItemContext>();

        private readonly Property<ContextPropertyItemContext> selectedItemProperty =
            new Property<ContextPropertyItemContext>();

        #endregion

        #region Constructors and Destructors

        public ContextPropertyContext()
        {
            this.items.Add(new ContextPropertyItemContext { Text = "This" });
            this.items.Add(new ContextPropertyItemContext { Text = "Is" });
            var itemContext = new ContextPropertyItemContext { Text = "Data Bind" };
            this.items.Add(itemContext);
            this.SelectedItem = itemContext;
        }

        #endregion

        #region Public Properties

        public void OnItemSelected(Context context)
        {
            Debug.Log("Item selected: " + context);
            this.SelectedItem = (ContextPropertyItemContext)context;
        }

        public Collection<ContextPropertyItemContext> Items
        {
            get
            {
                return this.items;
            }
        }

        public ContextPropertyItemContext SelectedItem
        {
            get
            {
                return this.selectedItemProperty.Value;
            }
            set
            {
                this.selectedItemProperty.Value = value;
            }
        }

        public Property<ContextPropertyItemContext> SelectedItemProperty
        {
            get
            {
                return this.selectedItemProperty;
            }
        }

        #endregion
    }
}