// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextPropertyItemContext.cs.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Examples.ContextProperty
{
    using Slash.Unity.DataBind.Core.Data;

    using UnityEngine;

    public class ContextPropertyItemContext : Context
    {
        #region Fields

        private readonly Property<string> textProperty = new Property<string>();

        #endregion

        #region Properties

        public string Text
        {
            get
            {
                return this.textProperty.Value;
            }
            set
            {
                this.textProperty.Value = value;
            }
        }

        public Property<string> TextProperty
        {
            get
            {
                return this.textProperty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void OnPrint()
        {
            Debug.Log("Printing selection: " + this);
        }

        public override string ToString()
        {
            return string.Format("Text: {0}", this.Text);
        }

        #endregion
    }
}