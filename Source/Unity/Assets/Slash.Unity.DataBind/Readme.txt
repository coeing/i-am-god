﻿// --------------------------------------------------------------------------------------------------------------------
// Data Bind for Unity v1.0
// Copyright (c) Slash Games. All rights reserved.
// --------------------------------------------------------------------------------------------------------------------

Setup
-----

- Import DataBind.unitypackage into your project (you probably already did if you read this file).

- If you are using NGUI, double click on the NGUIExtensions.unitypackage in DataBind/Addons to extract the NGUI specific scripts.

- If it's the first time you are using Data Bind for Unity:
-- Check the examples in DataBind/Examples
-- Read through the documentation at http://slashgames.org:8090/display/DBFU
-- The API of the classes is documented at http://slashgames.org/tools/data-bind/api

- If you encounter any issues (bugs, missing features,...) please create a new issue at the official issue tracker at https://bitbucket.org/coeing/data-bind/issues

- Any feedback, positive as well as negative, is always appreciated at contact@slashgames.org or at the official Unity forum thread at http://forum.unity3d.com/threads/data-bind-for-unity.283252/. Thanks for your help!

Changelog
---------
