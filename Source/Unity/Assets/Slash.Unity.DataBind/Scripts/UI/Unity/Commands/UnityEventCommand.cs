﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityEventCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.Unity.Commands
{
    using Slash.Unity.DataBind.Foundation.Commands;

    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    ///   Base class for a command which is called on a Unity event.
    /// </summary>
    /// <typeparam name="TBehaviour">Type of mono behaviour to observe for event.</typeparam>
    public abstract class UnityEventCommand<TBehaviour> : Command
        where TBehaviour : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Target to work with.
        /// </summary>
        public TBehaviour Target;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            if (this.Target == null)
            {
                this.Target = this.GetComponent<TBehaviour>();
            }
        }

        /// <summary>
        ///   Returns the event from the specified target to observe.
        /// </summary>
        /// <param name="target">Target behaviour to get event from.</param>
        /// <returns>Event from the specified target to observe.</returns>
        protected abstract UnityEvent GetEvent(TBehaviour target);

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected virtual void OnDisable()
        {
            if (this.Target != null)
            {
                var unityEvent = this.GetEvent(this.Target);
                unityEvent.RemoveListener(this.OnEvent);
            }
        }

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected virtual void OnEnable()
        {
            if (this.Target != null)
            {
                var unityEvent = this.GetEvent(this.Target);
                unityEvent.AddListener(this.OnEvent);
            }
        }

        /// <summary>
        ///   Called when the observed event occured.
        /// </summary>
        protected virtual void OnEvent()
        {
            this.InvokeCommand();
        }

        #endregion
    }
}