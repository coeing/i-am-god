﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GridItemsSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.Unity.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;

    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    ///   Set the items of a GridLayoutGroup depending on the items of the collection data value.
    /// </summary>
    [AddComponentMenu("Data Bind/Unity/Setters/[DB] Grid Items (Unity)")]
    public sealed class GridItemsSetter : GameObjectItemsSetter<GridLayoutGroup>
    {
    }
}