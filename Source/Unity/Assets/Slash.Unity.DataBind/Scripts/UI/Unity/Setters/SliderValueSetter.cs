﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SliderValueSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.Unity.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;

    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    ///   Set the value of a Slider depending on the data value.
    /// </summary>
    [AddComponentMenu("Data Bind/UnityUI/Setters/[DB] Slider Value (Unity)")]
    public class SliderValueSetter : BehaviourSingleSetter<Slider, float>
    {
        #region Methods

        protected override void OnValueChanged(float newValue)
        {
            this.Target.value = newValue;
        }

        #endregion
    }
}