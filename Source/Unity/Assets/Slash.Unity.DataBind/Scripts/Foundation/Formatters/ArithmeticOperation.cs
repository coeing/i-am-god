﻿namespace Slash.Unity.DataBind.Foundation.Formatters
{
    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEngine;

    [AddComponentMenu("Data Bind/Formatters/[DB] Arithmetic Operation")]
    public class ArithmeticOperation : DataProvider
    {
        public enum ArithmeticOperationType
        {
            None,

            Add,

            Sub,

            Multiply,

            Divide
        }

        #region Fields

        public DataBinding ArgumentA;

        public DataBinding ArgumentB;

        public ArithmeticOperationType Type;

        #endregion

        #region Properties

        public override object Value
        {
            get
            {
                float argumentA = this.ArgumentA.GetValue<float>();
                float argumentB = this.ArgumentB.GetValue<float>();

                float newValue = 0.0f;
                switch (this.Type)
                {
                    case ArithmeticOperationType.Add:
                        newValue = argumentA + argumentB;
                        break;
                    case ArithmeticOperationType.Sub:
                        newValue = argumentA - argumentB;
                        break;
                    case ArithmeticOperationType.Multiply:
                        newValue = argumentA * argumentB;
                        break;
                    case ArithmeticOperationType.Divide:
                        newValue = argumentA / argumentB;
                        break;
                }

                return newValue;
            }
        }

        #endregion

        #region Methods

        protected void Awake()
        {
            // Add bindings.
            this.AddBinding(this.ArgumentA);
            this.AddBinding(this.ArgumentB);
        }

        protected override void UpdateValue()
        {
            this.OnValueChanged(this.Value);
        }

        #endregion
    }
}