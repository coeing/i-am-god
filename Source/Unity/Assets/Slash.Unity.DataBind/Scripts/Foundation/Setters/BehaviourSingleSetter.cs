﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BehaviourSingleSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Foundation.Setters
{
    using UnityEngine;

    /// <summary>
    ///   Base class for a setter for a mono behaviour.
    /// </summary>
    /// <typeparam name="TBehaviour">Type of mono behaviour.</typeparam>
    /// <typeparam name="TData">Type of data to set.</typeparam>
    public abstract class BehaviourSingleSetter<TBehaviour, TData> : SingleSetter<TData>
        where TBehaviour : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Target widget.
        /// </summary>
        public TBehaviour Target;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            if (this.Target == null)
            {
                this.Target = this.GetComponent<TBehaviour>();
            }
        }

        #endregion
    }
}