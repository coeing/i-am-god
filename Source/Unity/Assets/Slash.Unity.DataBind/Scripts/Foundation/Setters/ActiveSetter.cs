﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Foundation.Setters
{
    using UnityEngine;

    /// <summary>
    ///   Setter which activates/deactivates a game object depending on the boolean data value.
    /// </summary>
    [AddComponentMenu("Data Bind/Core/Setters/[DB] Active")]
    public class ActiveSetter : GameObjectSingleSetter<bool>
    {
        #region Methods

        protected override void OnValueChanged(bool newValue)
        {
            this.Target.SetActive(newValue);
        }

        #endregion
    }
}