﻿namespace Slash.Unity.DataBind.Foundation.Setters
{
    using System.Collections.Generic;
    using System.Linq;

    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Core.Utils;

    using UnityEngine;

    [AddComponentMenu("Data Bind/Setters/[DB] Game Object Items")]
    public class GameObjectItemsSetter : GameObjectItemsSetter<MonoBehaviour>
    {
    }

    public abstract class GameObjectItemsSetter<TBehaviour> : ItemsSetter<TBehaviour>
        where TBehaviour : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Items.
        /// </summary>
        private readonly List<Item> items = new List<Item>();

        /// <summary>
        ///   Prefab to create the items from.
        /// </summary>
        public GameObject Prefab;

        #endregion

        #region Methods

        protected override void ClearItems()
        {
            this.Target.gameObject.DestroyChildren();
            this.items.Clear();
        }

        protected override void CreateItem(object itemContext)
        {
            GameObject item = this.Target.gameObject.AddChild(this.Prefab);

            if (itemContext != null)
            {
                // Set item data context.
                ContextHolder itemContextHolder = item.GetComponent<ContextHolder>();
                if (itemContextHolder == null)
                {
                    itemContextHolder = item.AddComponent<ContextHolder>();
                }
                itemContextHolder.Context = itemContext;
            }

            this.items.Add(new Item { GameObject = item, Context = itemContext });

            item.transform.SetParent(this.Target.transform, false);
        }

        protected override void RemoveItem(object itemContext)
        {
            // Get item.
            Item item = this.items.FirstOrDefault(existingItem => existingItem.Context == itemContext);
            if (item == null)
            {
                Debug.LogWarning("No item found for collection item " + itemContext, this);
                return;
            }

            // Destroy item.
            Destroy(item.GameObject);
        }

        #endregion

        private class Item
        {
            #region Properties

            public object Context { get; set; }

            public GameObject GameObject { get; set; }

            #endregion
        }
    }
}