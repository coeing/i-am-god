﻿namespace Slash.Unity.DataBind.Foundation.Checks
{
    using System;

    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEngine;

    [AddComponentMenu("Data Bind/Checks/[DB] Number Comparison Check")]
    public class ComparisonCheck : DataProvider
    {
        public enum ComparisonType
        {
            LessThan,

            Equal,

            GreaterThan
        }

        #region Fields

        public ComparisonType Comparison;

        public DataBinding First;

        public DataBinding Second;

        #endregion

        #region Properties

        public override object Value
        {
            get
            {
                // Assume first type is known, e.g. context property.
                var first = (IComparable)this.First.Value;
                if (first == null)
                {
                    return false;
                }

                // Convert second argument to type of first.
                var second = this.Second.Value != null ? Convert.ChangeType(this.Second.Value, first.GetType()) : null;

                // Compare values.
                var newValue = false;
                switch (this.Comparison)
                {
                    case ComparisonType.Equal:
                        newValue = first.CompareTo(second) == 0;
                        break;

                    case ComparisonType.GreaterThan:
                        newValue = first.CompareTo(second) > 0;
                        break;

                    case ComparisonType.LessThan:
                        newValue = first.CompareTo(second) < 0;
                        break;
                }

                return newValue;
            }
        }

        #endregion

        #region Methods

        protected override void UpdateValue()
        {
            this.OnValueChanged(this.Value);
        }

        private void Awake()
        {
            // Add bindings.
            this.AddBinding(this.First);
            this.AddBinding(this.Second);
        }

        #endregion
    }
}