﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BehaviourSingleGetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Foundation.Getters
{
    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEngine;

    /// <summary>
    ///   Base class for a getter for a mono behaviour which modifies a single data value.
    /// </summary>
    /// <typeparam name="TBehaviour">Type of mono behaviour to get value from.</typeparam>
    /// <typeparam name="TData">Type of data which is modified.</typeparam>
    public abstract class BehaviourSingleGetter<TBehaviour, TData> : DataProvider
        where TBehaviour : Component
    {
        #region Fields

        /// <summary>
        ///   Path to value in data context.
        /// </summary>
        public string Path;

        /// <summary>
        ///   Target behaviour.
        /// </summary>
        public TBehaviour Target;

        /// <summary>
        ///   Cache for contexts and master paths.
        /// </summary>
        private ContextNode node;

        #endregion

        #region Properties

        public override object Value
        {
            get
            {
                return this.Target != null ? this.GetValue(this.Target) : default(TData);
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void OnContextChanged()
        {
            base.OnContextChanged();

            if (this.node != null)
            {
                this.node.OnHierarchyChanged();

                // Update value.
                this.UpdateDataValue();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Register listener at target to be informed if its value changed.
        ///   The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to add listener to.</param>
        protected abstract void AddListener(TBehaviour target);

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected void Awake()
        {
            this.node = new ContextNode(this.gameObject, this.Path);

            if (this.Target == null)
            {
                this.Target = this.GetComponent<TBehaviour>();
            }
        }

        /// <summary>
        ///   Derived classes should return the current value to set if this method is called.
        ///   The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target behaviour to get value from.</param>
        /// <returns>Current value to set.</returns>
        protected abstract TData GetValue(TBehaviour target);

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected void OnDisable()
        {
            if (this.Target != null)
            {
                this.RemoveListener(this.Target);
            }
        }

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected void OnEnable()
        {
            if (this.Target != null)
            {
                this.AddListener(this.Target);
            }
        }

        /// <summary>
        ///   Has to be called by derived classes when the value may have changed.
        /// </summary>
        protected void OnTargetValueChanged()
        {
            this.UpdateDataValue();
            this.OnValueChanged(this.Value);
        }

        /// <summary>
        ///   Remove listener from target which was previously added in AddListener.
        ///   The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to remove listener from.</param>
        protected abstract void RemoveListener(TBehaviour target);

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected override void Start()
        {
            base.Start();

            // Initial update of data value.
            this.UpdateDataValue();

            // Inform that value changed.
            this.OnValueChanged(this.Value);
        }

        protected override void UpdateValue()
        {
            // Not required, data comes from presentation side.
        }

        private void UpdateDataValue()
        {
            this.node.SetValue(this.Value);
        }

        #endregion
    }
}