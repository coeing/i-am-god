﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionUtils.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Core.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    ///   Provides utility methods for reflecting types and members.
    /// </summary>
    public class ReflectionUtils
    {
        #region Public Methods and Operators

        /// <summary>
        ///   Creates a delegate of the specified type that represents the specified static or instance method, with the specified
        ///   first argument.
        /// </summary>
        /// <param name="type">The Type of delegate to create.</param>
        /// <param name="target">The object to which the delegate is bound, or null to treat method as static. </param>
        /// <param name="method">The MethodInfo describing the static or instance method the delegate is to represent.</param>
        /// <returns>A delegate of the specified type that represents the specified static or instance method. </returns>
        public static Delegate CreateDelegate(Type type, object target, MethodInfo method)
        {
            return Delegate.CreateDelegate(type, target, method);
        }

        /// <summary>
        ///   <para>
        ///     Looks up the specified full type name in all loaded assemblies,
        ///     ignoring assembly version.
        ///   </para>
        ///   <para>
        ///     In order to understand how to access generic types,
        ///     see http://msdn.microsoft.com/en-us/library/w3f99sx1.aspx.
        ///   </para>
        /// </summary>
        /// <param name="fullName">Full name of the type to find.</param>
        /// <returns>Type with the specified name.</returns>
        /// <exception cref="TypeLoadException">If the type couldn't be found.</exception>
        public static Type FindType(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return null;
            }

            // Split type name from .dll version.
            fullName = SystemExtensions.RemoveAssemblyInfo(fullName);

            Type t = Type.GetType(fullName);

            if (t != null)
            {
                return t;
            }

            foreach (Assembly asm in AssemblyUtils.GetLoadedAssemblies())
            {
                t = asm.GetType(fullName);
                if (t != null)
                {
                    return t;
                }
            }

            throw new TypeLoadException(string.Format("Unable to find type {0}.", fullName));
        }

        /// <summary>
        ///   Searches all loaded assemblies and returns the types which have the specified attribute.
        /// </summary>
        /// <returns>List of found types.</returns>
        /// <typeparam name="T">Type of the attribute to get the types of.</typeparam>
        public static IEnumerable<Type> FindTypesWithBase<T>() where T : class
        {
            List<Type> types = new List<Type>();
            Type baseType = typeof(T);
            foreach (Assembly assembly in AssemblyUtils.GetLoadedAssemblies())
            {
#if WINDOWS_STORE
                types.AddRange(
                    assembly.DefinedTypes.Where(baseType.GetTypeInfo().IsAssignableFrom)
                            .Select(typeInfo => typeInfo.AsType()));
#else
                types.AddRange(assembly.GetTypes().Where(baseType.IsAssignableFrom));
#endif
            }

            return types;
        }

        /// <summary>
        ///   Searches for the public field with the specified name.
        /// </summary>
        /// <param name="type">Type to search in.</param>
        /// <param name="name">The string containing the name of the data field to get.</param>
        /// <param name="bindingFlags">Binding flags to define how to search for field.</param>
        /// <returns>An object representing the public field with the specified name, if found; otherwise, null.</returns>
        public static FieldInfo GetField(Type type, string name, BindingFlags bindingFlags = BindingFlags.Default)
        {
            return type.GetField(name, bindingFlags);
        }

        /// <summary>
        ///   Searches for the public method with the specified name.
        /// </summary>
        /// <param name="type">Type to search in.</param>
        /// <param name="name">The string containing the name of the public method to get. </param>
        /// <returns>An object that represents the public method with the specified name, if found; otherwise, null.</returns>
        public static MethodInfo GetMethod(Type type, string name)
        {
            return type.GetMethod(name);
        }

        /// <summary>
        ///   Searches for the public property with the specified name.
        /// </summary>
        /// <param name="type">Type to search in.</param>
        /// <param name="name">The string containing the name of the public property to get. </param>
        /// <returns>An object that represents the public property with the specified name, if found; otherwise, null.</returns>
        public static PropertyInfo GetProperty(Type type, string name)
        {
            return type.GetProperty(name);
        }

        #endregion
    }
}