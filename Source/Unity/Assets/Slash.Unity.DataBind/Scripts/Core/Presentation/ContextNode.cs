﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextNode.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Core.Presentation
{
    using System;
    using System.Collections.Generic;

    using Slash.Unity.DataBind.Core.Data;

    using UnityEngine;

    /// <summary>
    ///   Node which works with a data context and caches contexts and master paths.
    ///   Always bound to a specific game object which specifies the hierarchy.
    /// </summary>
    public sealed class ContextNode
    {
        #region Constants

        private const int MaxPathDepth = 100500;

        #endregion

        #region Fields

        /// <summary>
        ///   Context cache for faster look up.
        /// </summary>
        private readonly Dictionary<int, ContextHolder> contexts = new Dictionary<int, ContextHolder>();

        /// <summary>
        ///   Game object to do the lookup for.
        /// </summary>
        private readonly GameObject gameObject;

        /// <summary>
        ///   Master path cache for faster look up.
        /// </summary>
        private readonly Dictionary<int, string> masterPaths = new Dictionary<int, string>();

        /// <summary>
        ///   Path in context this node is bound to.
        /// </summary>
        private readonly string path;

        private object context;

        /// <summary>
        ///   Callback when value changed.
        /// </summary>
        private Action<object> valueChangedCallback;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Constructor.
        /// </summary>
        /// <param name="gameObject">Game object this node is assigned to.</param>
        /// <param name="path">Path in context this node is bound to.</param>
        public ContextNode(GameObject gameObject, string path)
        {
            this.gameObject = gameObject;
            this.path = path;
            this.OnHierarchyChanged();
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Current context for this node.
        /// </summary>
        public object Context
        {
            get
            {
                return this.context;
            }
            private set
            {
                if (value == this.context)
                {
                    return;
                }

                // Remove listener from old context.
                this.RemoveListener();

                this.context = value;

                // Add listener to new context.
                var initialValue = this.RegisterListener();
                if (this.valueChangedCallback != null)
                {
                    this.valueChangedCallback(initialValue);
                }
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Informs the context node that the hierarchy changed, so the context and/or master paths may have changed.
        ///   Has to be called:
        ///   - Anchestor context changed.
        ///   - Anchestor master path changed.
        /// </summary>
        public void OnHierarchyChanged()
        {
            // Update master paths.
            this.UpdateMasterPaths();

            // Update context.
            this.Context = this.GetContext();
        }

        /// <summary>
        ///   Sets the specified value at the specified path.
        /// </summary>
        /// <param name="value">Value to set.</param>
        public void SetValue(object value)
        {
            // Set value on data context.
            var dataContext = this.context as Context;
            if (dataContext != null)
            {
                dataContext.SetValue(this.GetFullCleanPath(), value);
            }
        }

        /// <summary>
        ///   Sets the callback which is called when the value of the monitored data in the context changed.
        /// </summary>
        /// <param name="onValueChanged">Callback to invoke when the value of the monitored data in the context changed.</param>
        /// <returns>Initial value.</returns>
        public object SetValueListener(Action<object> onValueChanged)
        {
            // Remove old callback.
            this.RemoveListener();

            this.valueChangedCallback = onValueChanged;

            // Add new callback.
            return this.RegisterListener();
        }

        #endregion

        #region Methods

        private static ContextHolder FindRootContext(GameObject gameObject, int depthToGo)
        {
            ContextHolder lastContextHolder = null;
            var p = gameObject;
            depthToGo++;
            while (p != null && depthToGo > 0)
            {
                var context = p.GetComponent<ContextHolder>();
                if (context != null)
                {
                    lastContextHolder = context;
                    depthToGo--;
                }
                p = (p.transform.parent == null) ? null : p.transform.parent.gameObject;
            }
            return lastContextHolder;
        }

        private static string GetCleanPath(string path)
        {
            if (!path.StartsWith("#"))
            {
                return path;
            }
            var dotIndex = path.IndexOf('.');
            var result = (dotIndex < 0) ? path : path.Substring(dotIndex + 1);
            return result;
        }

        /// <summary>
        ///   Returns the data context for the specified path.
        /// </summary>
        /// <returns>Context for the specified path.</returns>
        private object GetContext()
        {
            ContextHolder contextHolder;
            if (!this.contexts.TryGetValue(GetPathDepth(this.path), out contextHolder))
            {
                var depthToGo = GetPathDepth(this.path);
                contextHolder = FindRootContext(this.gameObject, depthToGo);
                if (contextHolder != null)
                {
                    this.contexts.Add(depthToGo, contextHolder);
                }
            }
            return contextHolder != null ? contextHolder.Context : null;
        }

        /// <summary>
        ///   Converts the specified path to a full, clean path. I.e. replaces the depth value and prepends the master paths.
        /// </summary>
        /// <returns>Full clean path for the specified path.</returns>
        private string GetFullCleanPath()
        {
            var depthToGo = GetPathDepth(this.path);
            var cleanPath = GetCleanPath(this.path);

            string masterPath;
            if (this.masterPaths.TryGetValue(depthToGo, out masterPath) && !string.IsNullOrEmpty(masterPath))
            {
                return masterPath + "." + cleanPath;
            }

            return cleanPath;
        }

        private static int GetPathDepth(string path)
        {
            if (!path.StartsWith("#"))
            {
                return 0;
            }
            var depthString = path.Substring(1);
            var dotIndex = depthString.IndexOf('.');
            if (dotIndex >= 0)
            {
                depthString = depthString.Substring(0, dotIndex);
            }
            if (depthString == "#")
            {
                return MaxPathDepth;
            }
            int depth;
            if (int.TryParse(depthString, out depth))
            {
                return depth;
            }
            Debug.LogWarning("Failed to get binding context depth for: " + path);
            return 0;
        }

        /// <summary>
        ///   Registers a callback at the current context.
        /// </summary>
        /// <returns>Current value.</returns>
        private object RegisterListener()
        {
            var fullCleanPath = this.GetFullCleanPath();
            var dataContext = this.context as Context;
            if (dataContext != null && this.valueChangedCallback != null)
            {
                return dataContext.RegisterListener(fullCleanPath, this.valueChangedCallback);
            }

            // Return context itself if no path set, otherwise return null.
            return string.IsNullOrEmpty(fullCleanPath) ? this.context : null;
        }

        /// <summary>
        ///   Removes the callback from the current context.
        /// </summary>
        private void RemoveListener()
        {
            Context dataContext = this.context as Context;
            if (dataContext != null && this.valueChangedCallback != null)
            {
                // Remove listener.
                var fullCleanPath = this.GetFullCleanPath();
                dataContext.RemoveListener(fullCleanPath, this.valueChangedCallback);
            }
        }

        /// <summary>
        ///   Updates the master path cache.
        /// </summary>
        private void UpdateMasterPaths()
        {
            this.masterPaths.Clear();
            var p = this.gameObject;

            var lastAddedPath = "";
            var lastAddedDepth = -1;
            var depth = 0;

            while (p != null)
            {
                if (p.GetComponent<ContextHolder>() != null)
                {
                    depth++;
                }

                var masterPath = p.GetComponent<MasterPath>();
                if (masterPath != null)
                {
                    for (var d = lastAddedDepth + 1; d < depth; ++d)
                    {
                        this.masterPaths.Add(d, lastAddedPath);
                    }
                    lastAddedPath = masterPath.GetFullPath();
                    lastAddedDepth = depth;
                    if (!this.masterPaths.ContainsKey(lastAddedDepth))
                    {
                        this.masterPaths.Add(lastAddedDepth, lastAddedPath);
                    }
                }
                p = (p.transform.parent == null) ? null : p.transform.parent.gameObject;
            }
        }

        #endregion
    }
}