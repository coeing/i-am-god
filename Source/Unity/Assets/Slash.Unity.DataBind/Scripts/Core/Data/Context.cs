﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Context.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    using Slash.Unity.DataBind.Core.Utils;

    /// <summary>
    ///   Base class for a data context which contains properties to bind to.
    /// </summary>
    public abstract class Context
    {
        #region Fields

        /// <summary>
        ///   Root data node.
        /// </summary>
        private readonly DataNode root;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Constructor.
        /// </summary>
        protected Context()
        {
            this.root = new DataNode { Value = this };
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Registers a callback at the specified path of the context.
        /// </summary>
        /// <param name="path">Path to register for.</param>
        /// <param name="onValueChanged">Callback to invoke when value at the specified path changed.</param>
        /// <returns>Current value at specified path.</returns>
        public object RegisterListener(string path, Action<object> onValueChanged)
        {
            DataNode node = this.root.FindDescendant(path);

            // Register for value change.
            node.ValueChanged += onValueChanged;

            return node.Value;
        }

        /// <summary>
        ///   Removes the callback from the specified path of the context.
        /// </summary>
        /// <param name="path">Path to remove callback from.</param>
        /// <param name="onValueChanged">Callback to remove.</param>
        public void RemoveListener(string path, Action<object> onValueChanged)
        {
            DataNode node = this.root.FindDescendant(path);

            // Remove from value change.
            node.ValueChanged -= onValueChanged;
        }

        /// <summary>
        ///   Sets the specified value at the specified path.
        /// </summary>
        /// <param name="path">Path to set the data value at.</param>
        /// <param name="value">Value to set.</param>
        public void SetValue(string path, object value)
        {
            var node = this.root.FindDescendant(path);
            node.Value = value;
        }

        #endregion

        /// <summary>
        ///   Wraps a data object in the context tree and makes sure that the registered listeners are informed
        ///   when the data value changed.
        /// </summary>
        private sealed class DataNode
        {
            #region Fields

            /// <summary>
            ///   Data property to get informed if value changes.
            /// </summary>
            private Property property;

            private object value;

            #endregion

            #region Constructors and Destructors

            public DataNode()
            {
                this.Children = new List<DataNode>();
            }

            #endregion

            #region Events

            public event Action<object> ValueChanged;

            #endregion

            #region Properties

            public object Value
            {
                get
                {
                    return this.value;
                }
                set
                {
                    if (value == this.value)
                    {
                        return;
                    }

                    this.value = value;

                    // Set property value.
                    if (this.property != null)
                    {
                        this.property.Value = value;
                    }

                    this.OnValueChanged(this.value);

                    // Update children.
                    foreach (var childNode in this.Children)
                    {
                        childNode.UpdateContent(this.Value);
                    }
                }
            }

            private List<DataNode> Children { get; set; }

            private string Name { get; set; }

            private Property Property
            {
                get
                {
                    return this.property;
                }
                set
                {
                    if (value == this.property)
                    {
                        return;
                    }

                    if (this.property != null)
                    {
                        this.property.ValueChanged -= this.OnPropertyValueChanged;
                    }

                    this.property = value;

                    if (this.property != null)
                    {
                        this.property.ValueChanged += this.OnPropertyValueChanged;
                    }
                }
            }

            #endregion

            #region Public Methods and Operators

            public DataNode FindDescendant(string path)
            {
                var pointPos = path.IndexOf('.');
                string nodeName = path;
                string pathRest = null;
                if (pointPos >= 0)
                {
                    nodeName = path.Substring(0, pointPos);
                    pathRest = path.Substring(pointPos + 1);
                }

                // Get children with name.
                var childNode = this.GetChild(nodeName);
                return string.IsNullOrEmpty(pathRest) ? childNode : childNode.FindDescendant(pathRest);
            }

            #endregion

            #region Methods

            private DataNode CreateChild(string name)
            {
                var childNode = new DataNode() { Name = name };
                childNode.UpdateContent(this.Value);
                this.Children.Add(childNode);
                return childNode;
            }

            private DataNode GetChild(string name)
            {
                var childNode = this.Children.FirstOrDefault(child => child.Name == name) ?? this.CreateChild(name);
                return childNode;
            }

            private static Property GetProperty(object obj, string name)
            {
                if (obj == null)
                {
                    return null;
                }

                // Check for lower case field.
                var lowerCaseName = Char.ToLowerInvariant(name[0]) + name.Substring(1) + "Property";
                var propertyField = ReflectionUtils.GetField(
                    obj.GetType(),
                    lowerCaseName,
                    BindingFlags.Default | BindingFlags.NonPublic | BindingFlags.Instance);
                if (propertyField != null)
                {
                    return propertyField.GetValue(obj) as Property;
                }

                // Check for public property.
                var propertyProperty = ReflectionUtils.GetProperty(obj.GetType(), name + "Property");
                if (propertyProperty != null)
                {
                    return propertyProperty.GetValue(obj, null) as Property;
                }

                // Check for field.
                propertyField = ReflectionUtils.GetField(
                    obj.GetType(),
                    name + "Property",
                    BindingFlags.Default | BindingFlags.NonPublic | BindingFlags.Instance);
                if (propertyField != null)
                {
                    return propertyField.GetValue(obj) as Property;
                }

                return null;
            }

            private static object GetValue(object obj, string name)
            {
                if (obj == null)
                {
                    return null;
                }

                // Get property.
                var reflectionProperty = ReflectionUtils.GetProperty(obj.GetType(), name);
                if (reflectionProperty != null)
                {
                    return reflectionProperty.GetValue(obj, null);
                }

                // Get delegate.
                var reflectionMethod = ReflectionUtils.GetMethod(obj.GetType(), name);
                if (reflectionMethod != null)
                {
                    var args = new List<Type>(reflectionMethod.GetParameters().Select(p => p.ParameterType));
                    var delegateType = Expression.GetActionType(args.ToArray());
                    return ReflectionUtils.CreateDelegate(delegateType, obj, reflectionMethod);
                }

                return null;
            }

            private void OnPropertyValueChanged()
            {
                // Update value.
                this.Value = this.Property.Value;
            }

            private void OnValueChanged(object obj)
            {
                var handler = this.ValueChanged;
                if (handler != null)
                {
                    handler(obj);
                }
            }

            private void UpdateContent(object parentObject)
            {
                // Get property of the node.
                this.Property = GetProperty(parentObject, this.Name);

                // Get object of the node.
                this.Value = GetValue(parentObject, this.Name);
            }

            #endregion
        }
    }
}