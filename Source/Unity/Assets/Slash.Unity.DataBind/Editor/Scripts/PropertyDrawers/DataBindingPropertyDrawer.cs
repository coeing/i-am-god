﻿namespace Slash.Unity.DataBind.Editor.Scripts.PropertyDrawers
{
    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEditor;

    using UnityEngine;

    [CustomPropertyDrawer(typeof(DataBinding))]
    public class DataBindingPropertyDrawer : PropertyDrawer
    {
        #region Constants

        private const float LineHeight = 18f;

        #endregion

        #region Public Methods and Operators

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 16f + LineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
            SerializedProperty targetTypeProperty = property.FindPropertyRelative("Type");
            if (targetTypeProperty != null)
            {
                //Enum newTargetType = EditorGUI.EnumPopup(position, targetTypeProperty.intValue);
                contentPosition.height = 16f;
                EditorGUI.PropertyField(contentPosition, targetTypeProperty, GUIContent.none);
                EditorGUI.indentLevel++;
                position.y += LineHeight;
                // Check if correct type.
                DataBindingType targetType = (DataBindingType)targetTypeProperty.enumValueIndex;
                switch (targetType)
                {
                    case DataBindingType.Context:
                        {
                            Rect rect = new Rect(position) { height = LineHeight };
                            SerializedProperty pathProperty = property.FindPropertyRelative("Path");
                            EditorGUI.PropertyField(rect, pathProperty, new GUIContent("Path"));
                        }
                        break;
                    case DataBindingType.Provider:
                        {
                            Rect rect = new Rect(position) { height = LineHeight };
                            SerializedProperty providerProperty = property.FindPropertyRelative("Provider");
                            EditorGUI.PropertyField(rect, providerProperty, new GUIContent("Provider"));
                        }
                        break;
                    case DataBindingType.Constant:
                        {
                            Rect rect = new Rect(position) { height = LineHeight };
                            SerializedProperty targetProperty = property.FindPropertyRelative("Constant");
                            EditorGUI.PropertyField(rect, targetProperty, new GUIContent("Constant"));
                        }
                        break;
                }
            }
            EditorGUI.indentLevel = 0;
            EditorGUI.EndProperty();
        }

        #endregion
    }
}