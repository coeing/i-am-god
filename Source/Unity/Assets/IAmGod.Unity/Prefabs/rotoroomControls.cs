﻿using UnityEngine;
using System.Collections;

public class rotoroomControls : MonoBehaviour {

	public GameObject menu;

	// Use this for initialization
	void Start () {
		menu.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (Time.timeScale == 1.0)
			{
				Time.timeScale = 0;
				menu.SetActive(true);
			}
			else
			{
				Time.timeScale = 1;
				menu.SetActive(false);

			}
		}
	}

	public void play () {

		Time.timeScale = 1;
		menu.SetActive (false);
	}
}
