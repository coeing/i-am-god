﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LimitedLifetime.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts
{
    using System.Collections;

    using UnityEngine;

    public class LimitedLifetime : MonoBehaviour
    {
        #region Fields

        public float LifeTimeInSeconds;

        #endregion

        // Use this for initialization

        #region Methods

        /// <summary>
        ///   Destroies the fire ball.
        /// </summary>
        /// <returns>The fire ball.</returns>
        private IEnumerator DestroyGameObject()
        {
            yield return new WaitForSeconds(this.LifeTimeInSeconds);

            // Bloddy Hack: Move away to exit all triggers/colliders.
            this.transform.position = new Vector3(0, -1234567.89f, 0);

            yield return new WaitForSeconds(0.2f);

            Destroy(this.gameObject);
        }

        private void Start()
        {
            this.StartCoroutine(this.DestroyGameObject());
        }

        #endregion
    }
}