﻿using UnityEngine;
using System.Collections;

namespace IAmGod.Unity.Scripts
{
    public class MoveInRandomDirection : MonoBehaviour
    {
        #region Fields
    
        public Vector3 Direction;
        
        public float Speed;
        
        #endregion
        
        #region Methods
        
        private void FixedUpdate()
        {
//            this.gameObject.transform.position += this.Direction * Time.deltaTime * this.Speed;
            this.gameObject.rigidbody.AddForce(this.Direction * Time.deltaTime * this.Speed);
        }
        
        #endregion
    }
}
