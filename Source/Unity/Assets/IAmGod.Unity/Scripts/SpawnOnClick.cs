// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpawnOnClick.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts
{
    using IAmGod.Unity.Scripts.Input;

    using UnityEngine;

    /* This is the script we are going to use for the spawning powers on the screen */

    public class SpawnOnClick : MonoBehaviour
    {
        #region Fields

        public float CooldownInSeconds;

        public string IgnoreTagIfPresent;

        public LayerMask Layer;

        public AudioClip LoopClip;

        public Transform Parent;

        public GameObject Prefab;

        private float cooldownRemaning;

        private GameObject loopAudioSourceObject;

        #endregion

        #region Methods

        /// <summary>
        ///   Spawns the game object.
        /// </summary>
        private void SpawnGameObject(Vector3 pointerPos)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(pointerPos);
            if (Physics.Raycast(ray, out hit, float.MaxValue, this.Layer))
            {
                // Check if already present.
                if (!string.IsNullOrEmpty(this.IgnoreTagIfPresent))
                {
                    var hitObjectTag = hit.collider.tag;
                    if (hitObjectTag.Equals(this.IgnoreTagIfPresent))
                    {
                        return;
                    }
                }

                // Spawn new instance.
                Vector3 position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                var instance = (GameObject)Instantiate(this.Prefab, position, Quaternion.identity);

                if (this.Parent == null)
                {
                    var parentObject = new GameObject(this.name + "_parent");
                    this.Parent = parentObject.transform;
                }

                // Reparent.
                instance.transform.parent = this.Parent;

                // Start spawn cooldown.
                this.cooldownRemaning = this.CooldownInSeconds;

                // Play looped clip.
                if (this.loopAudioSourceObject == null && this.LoopClip != null)
                {
                    this.loopAudioSourceObject = new GameObject(this.Prefab.name + "_LoopAudio");
                    this.loopAudioSourceObject.transform.parent = this.Parent;
                    this.loopAudioSourceObject.transform.position = position;

                    var audioSource = this.loopAudioSourceObject.AddComponent<AudioSource>();
                    audioSource.clip = this.LoopClip;
                    audioSource.playOnAwake = false;
                    audioSource.loop = true;
                    audioSource.Play();
                }
            }
        }

        private void Update()
        {
            this.cooldownRemaning -= Time.deltaTime;

            if (this.cooldownRemaning > 0)
            {
                // Early out if on cooldown.
                return;
            }

            // Check input for the A button or left mouse click
            if (InputManager.InputProvider.IsButtonPressed(0))
            {
                this.SpawnGameObject(InputManager.InputProvider.PointerPosition);
            }
        }

        #endregion
    }
}