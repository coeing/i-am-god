﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameEntityContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Contexts
{
    using Slash.ECS.Components;
    using Slash.ECS.Events;

    public class GameEntityContext : GameContext
    {
        #region Properties

        public int EntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public virtual void Init(EventManager eventManager, EntityManager entityManager, int entityId)
        {
            this.Init(eventManager, entityManager);

            this.EntityId = entityId;
        }

        #endregion
    }
}