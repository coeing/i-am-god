﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectGameContextsWithGame.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Contexts
{
    using Slash.ECS;
    using Slash.Unity.Common.ECS;
    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEngine;

    public class ConnectGameContextsWithGame : MonoBehaviour
    {
        #region Fields

        public ContextHolder ContextHolder;

        public GameBehaviour GameBehaviour;

        #endregion

        #region Methods

        protected void Awake()
        {
            if (this.ContextHolder == null)
            {
                this.ContextHolder = FindObjectOfType<ContextHolder>();
            }
            if (this.GameBehaviour == null)
            {
                this.GameBehaviour = FindObjectOfType<GameBehaviour>();
            }

            if (this.GameBehaviour != null)
            {
                this.GameBehaviour.GameChanged += this.OnGameChanged;
                if (this.GameBehaviour.Game != null)
                {
                    this.OnGameChanged(this.GameBehaviour.Game, null);
                }
            }
        }

        private void OnGameChanged(Game newGame, Game oldGame)
        {
            var gameContext = this.ContextHolder != null ? this.ContextHolder.Context as GameContext : null;
            if (gameContext != null)
            {
                if (oldGame != null)
                {
                    gameContext.DeInit();
                }

                gameContext.Init(
                    newGame != null ? newGame.EventManager : null,
                    newGame != null ? newGame.EntityManager : null);
            }
        }

        #endregion
    }
}