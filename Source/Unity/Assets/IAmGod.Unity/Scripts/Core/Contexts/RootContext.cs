﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RootContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Contexts
{
    using IAmGod.Unity.Features.Outcome.Contexts;
    using IAmGod.Unity.Scripts.Features.Outcome.Behaviours;

    public class RootContext : GameContext
    {
        #region Constructors and Destructors

        public RootContext()
        {
            this.Outcome = new OutcomeContext();
            this.GameOver = new GameOverContext();

            this.SubContexts.Add(this.Outcome);
            this.SubContexts.Add(this.GameOver);
        }

        #endregion

        #region Properties

        public OutcomeContext Outcome { get; set; }

        public GameOverContext GameOver { get; set; }

        #endregion
    }
}