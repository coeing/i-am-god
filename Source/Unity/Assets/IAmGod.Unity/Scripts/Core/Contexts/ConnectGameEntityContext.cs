﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectGameEntityContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Contexts
{
    using Slash.Unity.Common.ECS;
    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEngine;

    public class ConnectGameEntityContext : MonoBehaviour
    {
        #region Fields

        public ContextHolder ContextHolder;

        public EntityConfigurationBehaviour EntityBehaviour;

        #endregion

        #region Methods

        protected void Awake()
        {
            if (this.ContextHolder == null)
            {
                this.ContextHolder = FindObjectOfType<ContextHolder>();
            }
            if (this.EntityBehaviour == null)
            {
                this.EntityBehaviour = FindObjectOfType<EntityConfigurationBehaviour>();
            }

            if (this.EntityBehaviour != null)
            {
                this.EntityBehaviour.EntityCreated += this.OnEntityCreated;
                if (this.EntityBehaviour.EntityId != 0)
                {
                    this.OnEntityCreated();
                }
            }
        }

        private void OnEntityCreated()
        {
            var gameEntityContext = this.ContextHolder != null ? this.ContextHolder.Context as GameEntityContext : null;
            if (gameEntityContext != null)
            {
                gameEntityContext.Init(
                    this.EntityBehaviour.Game.EventManager,
                    this.EntityBehaviour.Game.EntityManager,
                    this.EntityBehaviour.EntityId);
            }
        }

        #endregion
    }
}