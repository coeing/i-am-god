﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntityComponentBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Behaviours
{
    using Slash.ECS.Components;
    using Slash.Unity.Common.ECS;

    public abstract class EntityComponentBehaviour<TComponent> : GameEventBehaviour
        where TComponent : IEntityComponent
    {
        #region Fields

        protected TComponent LogicComponent;

        protected EntityConfigurationBehaviour EntityConfigurationBehaviour;

        #endregion

        #region Properties

        public int EntityId
        {
            get
            {
                return this.EntityConfigurationBehaviour != null ? this.EntityConfigurationBehaviour.EntityId : 0;
            }
        }

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();
            this.EntityConfigurationBehaviour = this.GetComponentInParent<EntityConfigurationBehaviour>();
        }

        protected virtual void OnInitialized()
        {
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        protected virtual void Update()
        {
            if (this.LogicComponent == null)
            {
                if (this.EntityConfigurationBehaviour.EntityId > 0)
                {
                    this.LogicComponent =
                        this.Game.EntityManager.GetComponent<TComponent>(this.EntityConfigurationBehaviour.EntityId);
                    if (this.LogicComponent != null)
                    {
                        this.OnInitialized();
                    }
                }
            }
        }

        #endregion
    }
}