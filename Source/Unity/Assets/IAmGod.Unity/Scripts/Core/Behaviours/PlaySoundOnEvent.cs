﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlaySoundOnEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Behaviours
{
    using IAmGod.Unity.Features.Fire.Components;

    using Slash.ECS.Events;

    using UnityEngine;

    public abstract class PlaySoundOnEvent<TEventType> : EntityComponentBehaviour<InflammableComponent>
    {
        #region Fields

        public AudioSource AudioSource;

        public AudioClip Clip;

        public TEventType EventTypeStart;

        public TEventType EventTypeStop;

        #endregion

        #region Methods

        protected abstract bool CheckEventData(object eventType, object eventData);

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(this.EventTypeStart, this.OnStartEvent);
            this.RegisterListener(this.EventTypeStop, this.OnStopEvent);
        }

        private void OnStopEvent(GameEvent e)
        {
            if (this.CheckEventData(e.EventType, e.EventData))
            {
                this.AudioSource.Stop();
            }
        }

        private void OnStartEvent(GameEvent e)
        {
            if (this.CheckEventData(e.EventType, e.EventData))
            {
                this.AudioSource.clip = this.Clip;
                this.AudioSource.loop = true;
                this.AudioSource.Play();
            }
        }
        #endregion
    }
}