﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetBoolAnimatorOnEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Core.Behaviours
{
    using IAmGod.Unity.Features.Fire.Components;

    using Slash.ECS.Events;

    using UnityEngine;

    public abstract class SetBoolAnimatorOnEvent<TEventType> : EntityComponentBehaviour<InflammableComponent>
    {
        #region Fields

        public Animator Animator;

        public string AnimatorKey;

        public TEventType EventTypeFalse;

        public TEventType EventTypeTrue;

        #endregion

        #region Methods

        protected abstract bool CheckEventData(object eventType, object eventData);

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(this.EventTypeTrue, this.OnEvent);
            this.RegisterListener(this.EventTypeFalse, this.OnEvent);
        }

        private void OnEvent(GameEvent e)
        {
            if (Equals(e.EventType, this.EventTypeTrue))
            {
                if (this.CheckEventData(e.EventType, e.EventData))
                {
                    this.Animator.SetBool(this.AnimatorKey, true);
                }
            }
            if (Equals(e.EventType, this.EventTypeFalse))
            {
                if (this.CheckEventData(e.EventType, e.EventData))
                {
                    this.Animator.SetBool(this.AnimatorKey, false);
                }
            }
        }

        #endregion
    }
}