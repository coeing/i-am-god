﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class WiiMote : MonoBehaviour {
	
	[DllImport ("UniWii")]
	private static extern void wiimote_start();
	
	[DllImport ("UniWii")]
	private static extern void wiimote_stop();
	
	[DllImport ("UniWii")]
	private static extern int wiimote_count();
	
	[DllImport ("UniWii")]
	private static extern byte wiimote_getAccX(int which);
	[DllImport ("UniWii")]
	private static extern byte wiimote_getAccY(int which);
	[DllImport ("UniWii")]
	private static extern byte wiimote_getAccZ(int which);
	
	/* The IR Returns a float (from -1 to 1) of the value of the IR sensors. 
	 * (-1,-1) is topleft, and (1,1) is bottom right.
	 * (0, 0) is the center of the screen in Ir coordinates
	 * (-1, 0) left center, (1, 0) right center
	 * (0, -1) center top, (0, 1) center bottom
	 * A value of -100 means the IR sensor could not be seen 
	 * (i.e. the IR LED was occluded, or the wiimote was pointed away, etc.)  */
	[DllImport ("UniWii")]
	private static extern float wiimote_getIrX(int which);
	[DllImport ("UniWii")]
	private static extern float wiimote_getIrY(int which);

	[DllImport ("UniWii")]
	private static extern float wiimote_getRoll(int which);
	[DllImport ("UniWii")]
	private static extern float wiimote_getPitch(int which);
	[DllImport ("UniWii")]
	private static extern float wiimote_getYaw(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonA(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonB(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonUp(int which);
	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonLeft(int which);
	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonRight(int which);
	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonDown(int which);

//	[DllImport ("UniWii")]	
//	private static extern bool wiimote_enableIR( int which );

	// Display the info of the wiimote data
	private string display;
	// The x and y position of the pointer ingame
	private int cursor_x, cursor_y;
	// The texture to draw ingame through OnGUI() [OnGUI is deprecated]
	public Texture2D cursor_tex;
	// The ingame cursor to control in the scene
	public GameObject ingameCursor;
	// oldVec is use in applyRPYValuesToModel()
	private Vector3 oldVec;

	// A variable to store the values of the accelerometer
	Vector3 accValues;
	// A variable to store the roll
	float roll;
	// A variable to store the pitch
	float pitch;
	// A variable to store the yaw (always 0, no wiimote plus support)
	float yaw;
	// A variable to store the IR values
	Vector2 irValues;
	
	// The bool for knowing where is the sensor bar (put value throught editor)
	public bool sensorBar_below;

	// The gameObject to send messages
	public GameObject objectToMessage;
	

	// Use this for initialization
	void Start () {
		wiimote_start();
		this.ingameCursor.SetActive(false);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// We count how many wiimotes there are connected
		int c = wiimote_count();
		if (c>0) {
			display = "";
			// We go through all the wiimotes with a for, and make them do the same
			for (int i=0; i<=c-1; i++) {
				CalculateAccelerometerValues(i);
				CalculateRollPitchYaw(i);
				CalculateIrPos(i);

//				display += "Wiimote " + i + " accX: " + this.accValues.x + " accY: " + this.accValues.y + " accZ: " + this.accValues.z +
//					" roll: " + this.roll + " pitch: " + this.pitch + " yaw: " + this.yaw + 
//						" IR X: " + this.irValues.x + " IR Y: " + this.irValues.y + "\n";

				DrawPointer(i);

				// This is my code for detecting when A is pressed
				bool aux = wiimote_getButtonA(i);
				objectToMessage.SendMessage("setButtonA", aux);

				// The code for the D-Pad
				bool buttonUp = wiimote_getButtonUp(i);
				bool buttonDown = wiimote_getButtonDown(i);
				bool buttonLeft = wiimote_getButtonLeft(i);
				bool buttonRight = wiimote_getButtonRight(i);
				objectToMessage.SendMessage("setButtonUp", buttonUp);
				objectToMessage.SendMessage("setButtonDown", buttonDown);
				objectToMessage.SendMessage("setButtonLeft", buttonLeft);
				objectToMessage.SendMessage("setButtonRight", buttonRight);

				bool buttonB = wiimote_getButtonB(i);
				objectToMessage.SendMessage("setButtonB", buttonB);

				
				
				if (aux) {
					//Debug.Log("A button pressed");
					//this.SendMessage("setButtonA", true);
				} else {

				}
			}
		}
//		else display = "Press the '1' and '2' buttons on your Wii Remote.";
	}

	// This function calculates the accelerometer values from the wiimote
	void CalculateAccelerometerValues (int i) {
		this.accValues = new Vector3 (wiimote_getAccX(i) , wiimote_getAccY(i), wiimote_getAccZ(i));
	}

	// This function calculates the roll, pitch, yaw values form the wiimote
	void CalculateRollPitchYaw (int i) {
		this.roll = Mathf.Round(wiimote_getRoll(i));
		this.pitch = Mathf.Round(wiimote_getPitch(i));
		this.yaw = Mathf.Round(wiimote_getYaw(i));
		//float yaw = wiimote_getYaw(i);
	}

	// This function calculates the IR (x,y) values form the wiimote
	void CalculateIrPos (int i) {
		this.irValues = new Vector3(wiimote_getIrX(i) , wiimote_getIrY(i));
	}

	// This function applies the values we got from RPY to a 3D model
	void applyRPYValuesToModel (string objectName) {
		// This is the code for moving in space a 3d model according to the values we get
		if (!float.IsNaN(roll) && !float.IsNaN(pitch)) {
			Vector3 vec = new Vector3(pitch, yaw , -1 * roll);
			vec = Vector3.Lerp(oldVec, vec, Time.deltaTime * 5);
			oldVec = vec;
			GameObject.Find(objectName).transform.eulerAngles = vec;
		}
	}
	
	// This function draws a pointer on Screen
	void DrawPointer (int i) {
		// This is the code for the pointer in screen space
		// This code is executed if the wiimote is pointing the screen
		// This code calculates a value for the variables cursor_x and cursor_y. We will draw them on screen
		if ( ((irValues.x != -100) && (irValues.y != -100)) ) {
			//If we are pointing the screen, we send a message to other script
			objectToMessage.SendMessage("setWiimoteOnScreen", true);
			this.ingameCursor.SetActive(true);

			float temp_x;	
			float temp_y;
			// We calculate the position of the cursor depending of the position of the sensor bar
			if (sensorBar_below) {
				// Sensor bar BELOW algorithm
				temp_x = ( Screen.width / 2) + irValues.x * (float) Screen.width / (float)2.0;
				temp_y = Screen.height - (irValues.y * (float) Screen.height / (float)2.0);
			} else {
				// Sensor bar ABOVE algorithm
				temp_x = ((irValues.x + (float) 1.0)/ (float)2.0) * (float) Screen.width;
				temp_y = (float) Screen.height - (((irValues.y + (float) 1.0)/ (float)2.0) * (float) Screen.height);
			}

			cursor_x = Mathf.RoundToInt(temp_x);
			cursor_y = Mathf.RoundToInt(temp_y);
			//Debug.Log("X: " + cursor_x.ToString() + ", Y: " + cursor_y.ToString());

			//if ((cursor_x != 0) || (cursor_y != 0))
			// We draw a box with our cursor position (I needed to tweak the Y value)
			ingameCursor.transform.position = new Vector3 (cursor_x, (Screen.height) - cursor_y, 0);
			//ingameCursor.transform.position = Camera.main.scre new Vector3 (cursor_x, (Screen.height) - cursor_y, 0);
			
			// We prepare the send the pointer position in screen points so that the script StarMovement can work with it
			// I have translated the values of the ir firectly inteo viewport numbers, so that is more precise
			objectToMessage.SendMessage("SetPointerValues" , ingameCursor.transform.position);

		} else {
			//If we are not pointing the screen, we send a message to other script
			objectToMessage.SendMessage("setWiimoteOnScreen", false);
			this.ingameCursor.SetActive(false);
		}

	}
	
	void OnApplicationQuit() {
		wiimote_stop();
	}
	
	void OnGUI() {
		// Where the info if the values will be displayed
		//GUI.Label( new Rect(10,10, 500, 100), display);

//		// This is the code for the pointer in screen space
//		// First, we do a function depending on the position of the cursor calculated above. Don't really know why
//		if ((cursor_x != 0) || (cursor_y != 0)) GUI.Box ( new Rect (cursor_x, cursor_y, 50, 50), cursor_tex); //"Pointing\nHere");
//		// We count how many wiimotes there are connected
//		int c = wiimote_count();
//		// We go through all the wiimotes with a for, and make them do the same
//		for (int i=0; i<=c-1; i++) {
//			// Get the X position of the current wiimote pointer
//			float ir_x = wiimote_getIrX(i);
//			// Get the Y position of the current wiimote pointer
//			float ir_y = wiimote_getIrY(i);
//
//
//			// This code is executed if the wiimote is pointing the screen
//			// This code calculates a value for the variables cursor_x and cursor_y. We will draw them on OnGUI
//			// This code is if the sensor bar is BELOW the screen
//			if ( (i==c-1) && (ir_x != -100) && (ir_y != -100) ) {
//				//float temp_x = ((ir_x + (float) 1.0)/ (float)2.0) * (float) Screen.width;
//				//float temp_y = (float) Screen.height - (((ir_y + (float) 1.0)/ (float)2.0) * (float) Screen.height);
//				float temp_x = ( Screen.width / 2) + ir_x * (float) Screen.width / (float)2.0;
//				float temp_y = Screen.height - (ir_y * (float) Screen.height / (float)2.0);
//				cursor_x = Mathf.RoundToInt(temp_x);
//				cursor_y = Mathf.RoundToInt(temp_y);
//			}
//			
//			
//			// This code is if the sensor bar is ABOVE the screen
//			// We check if the WiiMote is pointing the sensor bar
//			if ( (ir_x != -100) && (ir_y != -100) ) {
//				// We get a temporal (x, y) position and... modify it a bit? (I guess is whether you put the sensor bar above the screen or velow the screen)
//				float temp_x = ((ir_x + (float) 1.0)/ (float)2.0) * (float) Screen.width;
//				float temp_y = (float) Screen.height - (((ir_y + (float) 1.0)/ (float)2.0) * (float) Screen.height);
//				// Returns an int rounded to the nearest integer.
//				temp_x = Mathf.RoundToInt(temp_x);
//				temp_y = Mathf.RoundToInt(temp_y);
//				//if ((cursor_x != 0) || (cursor_y != 0))
//				// We draw a box with our temporal x,y position
//				GUI.Box ( new Rect (temp_x, temp_y, 64, 64), "Pointing\nHere" + i);
//			}
//		}
	}
}