﻿using UnityEngine;
using System.Collections;
/* This script will take in charge of playing the sounds of
 things that can play for a long time (fire, rain, earth) */
public class AudioManager : MonoBehaviour {

	// The collider array to overwrite
	RaycastHit[] colliderArray;

	//The radius of the sphere to build
	public float sphereCheckerRadius;

	public GameObject fireAudio;

	public GameObject rainAudio;

	// Use this for initialization
	void Start () {
		//this.fireAudio.SetActive(false);
		this.rainAudio.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		this.colliderArray = Physics.SphereCastAll (this.transform.position, sphereCheckerRadius,
		                                            transform.forward);
	
		for (int i = 1; i < this.colliderArray.Length; i++)
		{
			CheckForFire(i);
			CheckForRain(i);
		}
	}

	void CheckForFire (int i) {
		Debug.Log("CheckFire launched");
		if (this.colliderArray[i].collider.CompareTag("Fire")) {
			this.fireAudio.SetActive (true);
		} else {
			this.fireAudio.SetActive (false);
		}
	}

	void CheckForRain (int i) {
		if (this.colliderArray[i].collider.CompareTag("Water")) {
			this.rainAudio.SetActive (true);
		} else {
			this.rainAudio.SetActive (false);
		}
	}

	// OnDrawGizmos is called every frame and draw a gizmo only visible through the editor view
	void OnDrawGizmos()
	{		
		Gizmos.DrawWireSphere(transform.position, sphereCheckerRadius);
	}
}
