﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdleSoundBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Audio
{
    using UnityEngine;

    public class IdleSoundBehaviour : MonoBehaviour
    {
        #region Fields

        public AudioSource AudioSource;

        public float IdleSoundCooldown;

        public AudioClip[] IdleSounds;

        private float idleSoundCooldownRemaining;

        #endregion

        #region Methods

        private void PlaySound()
        {
            if (this.AudioSource.isPlaying)
            {
                // Idle sounds have lowest priority.
                return;
            }

            // Play random sound.
            var randomSoundIndex = Random.Range(0, this.IdleSounds.Length);
            var randomSound = this.IdleSounds[randomSoundIndex];

            this.AudioSource.clip = randomSound;
            this.AudioSource.loop = false;
            this.AudioSource.Play();
        }

        private void Start()
        {
            this.idleSoundCooldownRemaining = this.IdleSoundCooldown;
        }

        private void Update()
        {
            if (this.idleSoundCooldownRemaining > 0)
            {
                this.idleSoundCooldownRemaining -= Time.deltaTime;

                if (this.idleSoundCooldownRemaining <= 0)
                {
                    this.PlaySound();
                    this.idleSoundCooldownRemaining = this.IdleSoundCooldown;
                }
            }
        }

        #endregion
    }
}