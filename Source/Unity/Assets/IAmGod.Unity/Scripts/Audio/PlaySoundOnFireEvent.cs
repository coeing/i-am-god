﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlaySoundOnFireEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace IAmGod.Unity.Scripts.Audio
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Events;

    public class PlaySoundOnFireEvent : PlaySoundOnEvent<FireEvent>
    {
        protected override bool CheckEventData(object eventType, object eventData)
        {
            int entityId = (int)eventData;
            return entityId == this.EntityId;
        }
    }
}