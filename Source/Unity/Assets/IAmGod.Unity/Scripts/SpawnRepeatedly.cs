﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpawnRepeatedly.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using IAmGod.Unity.Scripts;

using UnityEngine;

public class SpawnRepeatedly : MonoBehaviour
{
    #region Fields

    public Vector3 Direction;

    public float FirstSpawnTimeInSeconds;

    public GameObject Prefab;

    public float TimeBetweenSpawnsInSeconds;

    public bool isRandomDir;

    #endregion

    #region Methods

    private void SpawnObject()
    {
        // Create instance.
        GameObject instance =
            (GameObject)Instantiate(this.Prefab, this.gameObject.transform.position, Quaternion.identity);

        if (instance == null)
        {
            Debug.LogError("Missing prefab! Assign prefab reference for spawning.");
            return;
        }

        // Reparent.
        instance.transform.parent = this.transform;

        // Set direction.
        var moveInFixedDirection = instance.GetComponent<MoveInFixedDirection>();
        if (!isRandomDir && moveInFixedDirection != null)
        {
            instance.GetComponent<MoveInRandomDirection>().enabled = false;
            moveInFixedDirection.Direction = this.Direction;
        } else
        {
            float xValue = instance.GetComponent<MoveInFixedDirection>().Direction.x;
            instance.rigidbody.useGravity = true;
            instance.rigidbody.mass = 1f;
            var moveInRandomDirection = instance.GetComponent<MoveInRandomDirection>();
            float rand = Random.Range(-180f, 180f);
            moveInRandomDirection.Direction = new Vector3(xValue, 180f, rand);
            instance.GetComponent<MoveInFixedDirection>().enabled = false;
        }
    }

    private void Start()
    {
        //Spawn a single Fireball every half second.
        this.InvokeRepeating("SpawnObject", this.FirstSpawnTimeInSeconds, this.TimeBetweenSpawnsInSeconds);
    }

    #endregion
}