﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangePower.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using IAmGod.Unity.Scripts.Input;

using UnityEngine;

public class ChangePower : MonoBehaviour
{
    private void OnEnable()
    {
        InputManager.ButtonJustPressed += this.OnButtonJustPressed;
    }

    private void OnDisable()
    {
        InputManager.ButtonJustPressed -= this.OnButtonJustPressed;
    }
    private void OnButtonJustPressed(int index)
    {
        this.Earth.SetActive(false);
        this.Fire.SetActive(false);
        this.Water.SetActive(false);

        // 1 is buttonUp and A
        if (index == 1)
        {
            this.Earth.SetActive(true);
        }
        // 2 is buttonRight and S
        else if (index == 2)
        {
            this.Water.SetActive(true);
        }
        // 3 is buttonDown and D
        else if (index == 3)
        {
            this.Fire.SetActive(true);
        }
       
    }

    /* This script will control wich power is active
	 Put the script on the prophet gameObject */

    #region Fields

    public GameObject Earth;

    public GameObject Fire;

    public GameObject Water;

    #endregion

    #region Methods

    #endregion
}