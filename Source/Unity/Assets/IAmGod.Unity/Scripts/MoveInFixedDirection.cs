﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MoveInFixedDirection.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts
{
    using UnityEngine;

    public class MoveInFixedDirection : MonoBehaviour
    {
        #region Fields

        public Vector3 Direction;

        public float Speed;

        #endregion

        #region Methods

        private void Update()
        {
            this.gameObject.transform.position += this.Direction * Time.deltaTime * this.Speed;
        }

        #endregion
    }
}