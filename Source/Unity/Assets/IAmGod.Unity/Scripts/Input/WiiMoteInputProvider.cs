﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WiiMoteInputProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Input
{
    using UnityEngine;

    public class WiiMoteInputProvider : InputProvider
    {
        #region Fields

        private Vector3 pointerPosition;

        #endregion

        #region Public Properties

        public override Vector3 PointerPosition
        {
            get
            {
                return this.pointerPosition;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Function for setting the pointerPosition (should be in screen units)
        /// </summary>
        /// <param name="values"></param>
        private void SetPointerValues(Vector3 values)
        {
            this.pointerPosition = new Vector3(values.x, values.y);
        }

        /// <summary>
        ///   Function to set the ButtonA value
        /// </summary>
        /// <param name="value"></param>
        private void setButtonA(bool value)
        {
            this.ButtonPressed[0] = value;
        }

		private void setButtonB(bool value)
		{
			this.ButtonPressed[5] = value;
		}

        private void setButtonDown(bool value)
        {
            this.OnButtonJustPressed(4);
        }

        private void setButtonLeft(bool value)
        {
            this.OnButtonJustPressed(3);
        }

        private void setButtonRight(bool value)
        {
            this.OnButtonJustPressed(2);
        }

        private void setButtonUp(bool value)
        {
            this.OnButtonJustPressed(1);
        }

        #endregion
    }
}