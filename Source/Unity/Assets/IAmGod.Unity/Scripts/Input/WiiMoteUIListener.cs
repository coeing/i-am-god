﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WiiMoteUIListener.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Input
{
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class WiiMoteUIListener : MonoBehaviour
    {
        #region Fields

        private readonly List<UnityButton> buttons = new List<UnityButton>();

        #endregion

        #region Methods

        private void OnLevelWasLoaded()
        {
            // Discover UI controls.
            this.buttons.Clear();

            foreach (var button in FindObjectsOfType<Button>())
            {
                this.buttons.Add(
                    new UnityButton
                        {
                            Button = button,
                            Hovered = false,
                            RectTransform = button.GetComponent<RectTransform>()
                        });
            }
        }

        private void Update()
        {
            foreach (var button in this.buttons)
            {
                // Get button rect.
                Vector3[] buttonCorners = new Vector3[4];
                button.RectTransform.GetWorldCorners(buttonCorners);

                var buttonRect = new Rect(
                    buttonCorners[0].x,
                    buttonCorners[0].y,
                    buttonCorners[2].x - buttonCorners[0].x,
                    buttonCorners[2].y - buttonCorners[0].y);

                if (buttonRect.Contains(InputManager.InputProvider.PointerPosition))
                {
                    if (!button.Hovered)
                    {
                        // Hover.
                        button.Button.OnPointerEnter(null);
                        button.Hovered = true;
                    }

                    if (InputManager.InputProvider.IsButtonPressed(0))
                    {
                        // Click.
                        button.Button.OnPointerClick(new PointerEventData(FindObjectOfType<EventSystem>()));
                    }
                }
                else if (button.Hovered)
                {
                    // Unhover.
                    button.Hovered = false;
                    button.Button.OnPointerExit(null);
                }
            }
        }

        #endregion

        private class UnityButton
        {
            #region Public Properties

            public Button Button { get; set; }

            public bool Hovered { get; set; }

            public RectTransform RectTransform { get; set; }

            #endregion
        }
    }
}