﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ButtonClickSound.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Input
{
    using System.Collections.Generic;

    using Slash.Unity.Common.Scenes;

    using UnityEngine;
    using UnityEngine.UI;

    public class ButtonClickSound : MonoBehaviour
    {
        #region Fields

        public AudioSource AudioSource;

        private readonly List<Button> buttons = new List<Button>();

        #endregion

        #region Methods

        private void OnClick()
        {
            this.AudioSource.Play();
        }

        private void OnDestroy()
        {
            SceneManager.Instance.SceneChanging -= this.OnSceneChanging;
            SceneManager.Instance.SceneChanged -= this.OnSceneChanged;
        }

        private void OnSceneChanged(SceneRoot newSceneRoot)
        {
            // Add listeners.
            this.buttons.Clear();

            foreach (var button in FindObjectsOfType<Button>())
            {
                button.onClick.AddListener(this.OnClick);
                this.buttons.Add(button);
            }
        }

        private void OnSceneChanging(string newScene)
        {
            // Remove listeners.
            foreach (var button in this.buttons)
            {
                button.onClick.RemoveListener(this.OnClick);
            }
        }

        private void Start()
        {
            SceneManager.Instance.SceneChanging += this.OnSceneChanging;
            SceneManager.Instance.SceneChanged += this.OnSceneChanged;
        }

        #endregion
    }
}