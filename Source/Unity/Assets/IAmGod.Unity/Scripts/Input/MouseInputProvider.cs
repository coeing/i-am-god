﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MouseInputProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Input
{
    using UnityEngine;

    public class MouseInputProvider : InputProvider
    {
        #region Public Properties

        public override Vector3 PointerPosition
        {
            get
            {
                return Input.mousePosition;
            }
        }

        #endregion

        #region Methods

        private void Update()
        {
            this.ButtonPressed[0] = Input.GetMouseButton(0);
			// Right click
			this.ButtonPressed[5] = Input.GetMouseButton(1);

            if (Input.GetKeyDown(KeyCode.A))
            {
                this.OnButtonJustPressed(1);
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                this.OnButtonJustPressed(2);
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                this.OnButtonJustPressed(3);
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                this.OnButtonJustPressed(4);
            }
        }

        #endregion
    }
}