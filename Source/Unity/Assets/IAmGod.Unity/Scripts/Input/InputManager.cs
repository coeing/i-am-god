﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputManager.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Input
{
    using System;

    using UnityEngine;

    /// <summary>
    ///   Abstracts input from the underlying device (e.g. mouse, WiiMote).
    /// </summary>
    public class InputManager : MonoBehaviour
    {
        #region Static Fields

        private static InputProvider inputProvider;

        #endregion

        #region Fields

        public MouseInputProvider MouseInputProvider;

        public WiiMoteInputProvider WiiMoteInputProvider;

        #endregion

        #region Public Events

        public static event Action<int> ButtonJustPressed;

        #endregion

        #region Public Properties

        public static InputProvider InputProvider
        {
            get
            {
                return inputProvider;
            }
            set
            {
                if (inputProvider == value)
                {
                    return;
                }

                if (inputProvider != null)
                {
                    inputProvider.ButtonJustPressed -= OnButtonJustPressed;
                }

                inputProvider = value;

                if (inputProvider != null)
                {
                    inputProvider.ButtonJustPressed += OnButtonJustPressed;
                }
            }
        }

        #endregion

        #region Methods

        private static void OnButtonJustPressed(int index)
        {
            var handler = ButtonJustPressed;
            if (handler != null)
            {
                handler(index);
            }
        }

        private void Start()
        {
            this.setWiimoteOnScreen(false);
        }

        private void setWiimoteOnScreen(bool wiiMoteOnScreen)
        {
            if (wiiMoteOnScreen)
            {
                InputProvider = this.WiiMoteInputProvider;
            }
            else
            {
                InputProvider = this.MouseInputProvider;
            }
        }

        #endregion
    }
}