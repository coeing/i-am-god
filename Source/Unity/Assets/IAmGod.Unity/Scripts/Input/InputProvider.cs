﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Input
{
    using System;

    using UnityEngine;

    /// <summary>
    ///   Base class for device input handling.
    /// </summary>
    public abstract class InputProvider : MonoBehaviour
    {
        #region Constants

        private const int MaximumButtonCount = 16;

        #endregion

        #region Fields

        protected bool[] ButtonPressed = new bool[MaximumButtonCount];

        #endregion

        #region Public Events

        public event Action<int> ButtonJustPressed;

        #endregion

        #region Public Properties

        public abstract Vector3 PointerPosition { get; }

        #endregion

        #region Public Methods and Operators

        public bool IsButtonPressed(int buttonId)
        {
            return this.ButtonPressed[buttonId];
        }

        #endregion

        #region Methods

        protected void OnButtonJustPressed(int index)
        {
            var handler = this.ButtonJustPressed;
            if (handler != null)
            {
                handler(index);
            }
        }

        #endregion
    }
}