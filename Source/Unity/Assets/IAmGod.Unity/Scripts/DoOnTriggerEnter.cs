﻿using UnityEngine;
using System.Collections;

public class DoOnTriggerEnter : MonoBehaviour {

	// A float to kill this gameObject after x seconds
	float deadTime;
	float deadTimeRemaining;

	// Use this for initialization
	void Start () {
		deadTime = 3f;

		// Start dead cooldown.
		this.deadTimeRemaining = this.deadTime;
	}
	
	// Update is called once per frame
	void Update () {
	
		this.deadTimeRemaining -= Time.deltaTime;

		if (this.deadTimeRemaining < 0)
		{
			Destroy(this.gameObject);
		}

	}

	void OnTriggerEnter (Collider coll) {
		if (collider.CompareTag("NavMeshObstacle")) {
			Destroy(collider.gameObject);
			Debug.Log("Destroy navMesh Obstacle");
		}
	}
}
