﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditsControls.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace IAmGod.Unity.Scripts.Features.CreditsControls.Contexts
{
    using Slash.Unity.Common.Scenes;
    using Slash.Unity.DataBind.Core.Data;

    public class CreditsControlsContext : Context
    {
        public void OnBack()
        {
            SceneManager.Instance.ChangeScene("MainMenu");
        }
    }
}