﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainMenuContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.MainMenu.Contexts
{
    using Slash.Unity.Common.Scenes;
    using Slash.Unity.DataBind.Core.Data;

    using UnityEngine;

    public class MainMenuContext : Context
    {

        #region Public Methods and Operators

        public void OnCreditsControls()
        {
            SceneManager.Instance.ChangeScene("CreditsControls");
        }

        public void OnQuit()
        {
            Application.Quit();
        }

        public void OnStart()
        {
            SceneManager.Instance.ChangeScene("narratVillage01");
        }

        #endregion
    }
}