﻿namespace IAmGod.Unity.Features.Victory.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using IAmGod.Unity.Features.Victory.Events;
    using IAmGod.Unity.Features.Health.Events;
    using IAmGod.Unity.Features.Victory.Components;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;
    using Slash.Math.Utils;

    [GameSystem]
    public class VictorySystem : GameSystem
    {
        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);
            this.EventManager.RegisterListener(VictoryEvent.GoalEntered, this.OnGoalEntered);
        }

        private void OnGoalEntered(GameEvent e)
        {
            GoalEnteredData data = (GoalEnteredData)e.EventData;
            int enteringEntity = data.EntityId;
            ScorableComponent scorableComponent = this.EntityManager.GetComponent<ScorableComponent>(enteringEntity);

            if (scorableComponent != null)
            {
                this.EventManager.QueueEvent(VictoryEvent.Won);
            }
        }
    }
}
