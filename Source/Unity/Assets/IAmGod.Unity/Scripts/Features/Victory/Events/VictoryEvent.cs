﻿namespace IAmGod.Unity.Features.Victory.Events
{
    using Slash.ECS.Events;
    
    [GameEventType]
    public enum VictoryEvent
    {
        Won,

        GoalEntered,

        None,
    }
}
