﻿namespace IAmGod.Unity.Features.Victory.Events
{
    public class GoalEnteredData
    {
        /// <summary>
        ///   Goal which was entered.
        /// </summary>
        public int GoalEntityId { get; set; }
        
        /// <summary>
        ///   Id of entity which entered the goal.
        /// </summary>
        public int EntityId { get; set; }
    }
}
