﻿namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Victory.Components;
    using IAmGod.Unity.Features.Victory.Events;
    
    using Slash.Unity.Common.ECS;
    
    using UnityEngine;
    
    public class GoalBehaviour : EntityComponentBehaviour<GoalComponent>
    {
        protected void OnTriggerEnter(Collider other)
        {
            EntityConfigurationBehaviour entityBehaviour = other.GetComponentInParent<EntityConfigurationBehaviour>();
            if (entityBehaviour != null)
            {
                this.Game.EventManager.QueueEvent(
                    VictoryEvent.GoalEntered,
                    new GoalEnteredData { GoalEntityId = this.EntityId, EntityId = entityBehaviour.EntityId });
            }
        }
    }
}