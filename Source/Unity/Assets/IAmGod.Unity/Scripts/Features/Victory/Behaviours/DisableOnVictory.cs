﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DisableOnVictory.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Features.Victory.Events;

    using Slash.ECS.Events;
    using Slash.Unity.Common.ECS;

    public class DisableOnVictory : GameEventBehaviour
    {
        #region Methods

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(VictoryEvent.Won, this.OnWon);
        }

        private void OnWon(GameEvent e)
        {
            this.gameObject.SetActive(false);
        }

        #endregion
    }
}