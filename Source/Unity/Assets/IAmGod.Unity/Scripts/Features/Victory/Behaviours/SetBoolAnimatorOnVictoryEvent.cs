﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetBoolAnimatorOnVictoryEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Victory.Events;

    public class SetBoolAnimatorOnVictoryEvent : SetBoolAnimatorOnEvent<VictoryEvent>
    {
        #region Methods

        protected override bool CheckEventData(object eventType, object eventData)
        {
            return true;
        }

        #endregion
    }
}