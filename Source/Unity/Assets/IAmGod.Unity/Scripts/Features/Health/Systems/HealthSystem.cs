﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HealthSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Health.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using IAmGod.Unity.Features.Fire.Events;
    using IAmGod.Unity.Features.Health.Components;
    using IAmGod.Unity.Features.Health.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;
    using Slash.Math.Utils;

    [GameSystem]
    public class HealthSystem : GameSystem
    {
        #region Fields

        private readonly List<LivingActor> livingActors = new List<LivingActor>();

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EntityManager.RegisterComponentListeners<HealthComponent>(this.OnHealthAdded, this.OnHealthRemoved);

            this.EventManager.RegisterListener(FireEvent.Inflamed, this.OnInflamed);
            this.EventManager.RegisterListener(FireEvent.Extinguished, this.OnExtinguished);
        }

        public override void UpdateSystem(float dt)
        {
            base.UpdateSystem(dt);

            foreach (var livingActor in this.livingActors)
            {
                if (livingActor.HealthComponent.Health <= 0)
                {
                    continue;
                }

                float newHealth = livingActor.HealthComponent.Health + livingActor.HealthComponent.HealthChange * dt;

                // Clamp.
                newHealth = MathUtils.Clamp(newHealth, 0, livingActor.HealthComponent.MaxHealth);

                if (Math.Abs(newHealth - livingActor.HealthComponent.Health) > 0.01f)
                {
                    livingActor.HealthComponent.Health = newHealth;

                    this.EventManager.QueueEvent(HealthEvent.HealthChanged, livingActor.EntityId);

                    if (livingActor.HealthComponent.Health <= 0)
                    {
                        this.EventManager.QueueEvent(HealthEvent.Died, livingActor.EntityId);
                    }
                }
            }
        }

        #endregion

        #region Methods

        private LivingActor GetLivingActorByEntityId(int entityId)
        {
            return this.livingActors.FirstOrDefault(livingActor => livingActor.EntityId == entityId);
        }

        private void OnExtinguished(GameEvent e)
        {
            int entityId = (int)e.EventData;

            // Check if living actor.
            LivingActor livingActor = this.GetLivingActorByEntityId(entityId);
            if (livingActor == null)
            {
                return;
            }

            // Modify health change.
            livingActor.HealthComponent.HealthChange += livingActor.HealthComponent.FireDamage;
        }

        private void OnHealthAdded(int entityId, HealthComponent component)
        {
            this.livingActors.Add(new LivingActor() { EntityId = entityId, HealthComponent = component });
        }

        private void OnHealthRemoved(int entityId, HealthComponent component)
        {
            this.livingActors.RemoveAll(livingActor => livingActor.EntityId == entityId);
        }

        private void OnInflamed(GameEvent e)
        {
            int entityId = (int)e.EventData;

            // Check if living actor.
            LivingActor livingActor = this.GetLivingActorByEntityId(entityId);
            if (livingActor == null)
            {
                return;
            }

            // Modify health change.
            livingActor.HealthComponent.HealthChange -= livingActor.HealthComponent.FireDamage;
        }

        #endregion

        public class LivingActor
        {
            #region Properties

            public int EntityId { get; set; }

            public HealthComponent HealthComponent { get; set; }

            #endregion
        }
    }
}