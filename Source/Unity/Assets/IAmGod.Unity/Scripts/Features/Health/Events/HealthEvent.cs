namespace IAmGod.Unity.Features.Health.Events
{
    using Slash.ECS.Events;
    
    [GameEventType]
    public enum HealthEvent
    {
        HealthChanged,

        Died,
    }
}