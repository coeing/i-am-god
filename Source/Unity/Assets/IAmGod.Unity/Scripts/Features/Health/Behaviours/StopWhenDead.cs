﻿namespace IAmGod.Unity.Features.Health.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Components;
    using IAmGod.Unity.Features.Health.Components;
    using IAmGod.Unity.Features.Health.Events;

    using Slash.ECS.Events;

    using UnityEngine;

    public class StopWhenDead : EntityComponentBehaviour<HealthComponent>
    {
        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(HealthEvent.Died, this.OnDied);
        }

        public NavMeshAgent Agent;

        private void OnDied(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                // Disable agent.
                this.Agent.enabled = false;
            }
        }
    }
}
