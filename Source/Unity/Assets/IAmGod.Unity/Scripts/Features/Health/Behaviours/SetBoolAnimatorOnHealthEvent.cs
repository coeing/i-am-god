﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetBoolAnimatorOnHealthEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Health.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Health.Events;

    public class SetBoolAnimatorOnHealthEvent : SetBoolAnimatorOnEvent<HealthEvent>
    {
        #region Methods

        protected override bool CheckEventData(object eventType, object eventData)
        {
            int entityId = (int)eventData;
            return entityId == this.EntityId;
        }

        #endregion
    }
}