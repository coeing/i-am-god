﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeathBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Health.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Health.Components;
    using IAmGod.Unity.Features.Health.Events;
    using IAmGod.Unity.Scripts.Audio;

    using Slash.ECS.Events;

    using UnityEngine;

    public class DeathBehaviour : EntityComponentBehaviour<HealthComponent>
    {
        #region Fields

        public AudioSource AudioSource;

        public AudioClip[] DeathSounds;

        #endregion

        #region Methods

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(HealthEvent.Died, this.OnDied);
        }

        private void OnDied(GameEvent e)
        {
            // Play random death sound.
            var randomSoundIndex = Random.Range(0, this.DeathSounds.Length);
            var randomSound = this.DeathSounds[randomSoundIndex];

            this.AudioSource.clip = randomSound;
            this.AudioSource.loop = false;
            this.AudioSource.Play();

            var idleSoundBehaviour = this.GetComponentInChildren<IdleSoundBehaviour>();
            if (idleSoundBehaviour != null)
            {
                idleSoundBehaviour.enabled = false;
            }
        }

        #endregion
    }
}