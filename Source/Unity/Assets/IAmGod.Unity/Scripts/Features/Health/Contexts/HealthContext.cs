﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HealthContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Health.Contexts
{
    using IAmGod.Unity.Core.Contexts;
    using IAmGod.Unity.Features.Health.Components;
    using IAmGod.Unity.Features.Health.Events;

    using Slash.ECS.Components;
    using Slash.ECS.Events;
    using Slash.Unity.DataBind.Core.Data;

    public class HealthContext : GameEntityContext
    {
        #region Fields

        private readonly Property<float> healthProperty = new Property<float>();

        private readonly Property<int> maxHealthProperty = new Property<int>();

        private HealthComponent healthComponent;

        #endregion

        #region Properties

        public float Health
        {
            get
            {
                return this.healthProperty.Value;
            }
            set
            {
                this.healthProperty.Value = value;
            }
        }

        public int MaxHealth
        {
            get
            {
                return this.maxHealthProperty.Value;
            }
            set
            {
                this.maxHealthProperty.Value = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void Init(EventManager eventManager, EntityManager entityManager, int entityId)
        {
            base.Init(eventManager, entityManager, entityId);

            this.healthComponent = this.EntityManager.GetComponent<HealthComponent>(this.EntityId);
            if (this.healthComponent != null)
            {
                this.Health = this.healthComponent.Health;
                this.MaxHealth = this.healthComponent.MaxHealth;
            }
        }

        public override void SetEventListeners()
        {
            base.SetEventListeners();

            this.SetEventListener(HealthEvent.HealthChanged, this.OnHealthChanged);
        }

        #endregion

        #region Methods

        private void OnHealthChanged(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                this.Health = this.healthComponent.Health;
            }
        }

        #endregion
    }
}