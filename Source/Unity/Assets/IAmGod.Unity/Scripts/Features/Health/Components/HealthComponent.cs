﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HealthComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Health.Components
{
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class HealthComponent : EntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Fire damage (in hitpoints/s).
        /// </summary>
        public const string AttributeFireDamage = "HealthComponent.FireDamage";

        /// <summary>
        ///   Attribute: Current health.
        /// </summary>
        public const string AttributeHealth = "HealthComponent.Health";

        /// <summary>
        ///   Attribute: Maximum health.
        /// </summary>
        public const string AttributeMaxHealth = "HealthComponent.MaxHealth";

        /// <summary>
        ///   Attribute default: Fire damage (in hitpoints/s).
        /// </summary>
        public const float DefaultFireDamage = 10.0f;

        /// <summary>
        ///   Attribute default: Current health.
        /// </summary>
        public const float DefaultHealth = 0;

        /// <summary>
        ///   Attribute default: Maximum health.
        /// </summary>
        public const int DefaultMaxHealth = 100;

        #endregion

        #region Constructors and Destructors

        public HealthComponent()
        {
            this.Health = DefaultHealth;
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Fire damage (in hitpoints/s).
        /// </summary>
        [InspectorFloat(AttributeFireDamage, Default = DefaultFireDamage, Description = "Fire damage (in hitpoints/s).")
        ]
        public float FireDamage { get; set; }

        /// <summary>
        ///   Current health.
        /// </summary>
        [InspectorFloat(AttributeHealth, Default = DefaultHealth, Description = "Current health.")]
        public float Health { get; set; }

        /// <summary>
        ///   Maximum health.
        /// </summary>
        [InspectorInt(AttributeMaxHealth, Default = DefaultMaxHealth, Description = "Maximum health.")]
        public int MaxHealth { get; set; }

        /// <summary>
        ///   Current health change (in hitpoints/s).
        /// </summary>
        public float HealthChange { get; set; }

        #endregion
    }
}