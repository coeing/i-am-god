﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FollowerSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Followers.Systems
{
    using System.Collections.Generic;
    using System.Linq;

    using IAmGod.Unity.Features.Followers.Components;
    using IAmGod.Unity.Features.Followers.Events;
    using IAmGod.Unity.Features.Health.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;

    [GameSystem]
    public class FollowerSystem : GameSystem
    {
        #region Fields

        private readonly List<Follower> followers = new List<Follower>();

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(HealthEvent.Died, this.OnDied);

            this.EntityManager.RegisterComponentListeners<FollowerComponent>(
                this.OnFollowerAdded,
                this.OnFollowerRemoved);
        }

        #endregion

        #region Methods

        private Follower GetFollower(int entityId)
        {
            return this.followers.FirstOrDefault(follower => follower.EntityId == entityId);
        }

        private void OnDied(GameEvent e)
        {
            int entityId = (int)e.EventData;

            // Check if follower.
            Follower follower = this.GetFollower(entityId);
            if (follower == null)
            {
                return;
            }

            // Remove follower.
            //this.EntityManager.RemoveEntity(follower.EntityId);
            this.followers.Remove(follower);
            this.EventManager.QueueEvent(FollowerEvent.FollowerRemoved, entityId);
        }

        private void OnFollowerAdded(int entityId, FollowerComponent component)
        {
            Follower follower = new Follower() { EntityId = entityId };
            this.followers.Add(follower);
            this.EventManager.QueueEvent(FollowerEvent.FollowerAdded, entityId);
        }

        private void OnFollowerRemoved(int entityId, FollowerComponent component)
        {
           // this.followers.RemoveAll(follower => follower.EntityId == entityId);
           // this.EventManager.QueueEvent(FollowerEvent.FollowerRemoved, entityId);
        }

        #endregion

        public class Follower
        {
            #region Properties

            public int EntityId { get; set; }

            #endregion
        }
    }
}