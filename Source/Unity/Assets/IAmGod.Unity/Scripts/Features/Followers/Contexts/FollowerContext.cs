﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FollowerContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Followers.Contexts
{
    using IAmGod.Unity.Core.Contexts;
    using IAmGod.Unity.Features.Health.Contexts;

    using Slash.ECS.Components;
    using Slash.ECS.Events;

    public class FollowerContext : GameEntityContext
    {
        #region Constructors and Destructors

        public FollowerContext()
        {
            this.Health = new HealthContext();
        }

        #endregion

        #region Properties

        public HealthContext Health { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(EventManager eventManager, EntityManager entityManager, int entityId)
        {
            base.Init(eventManager, entityManager, entityId);

            this.Health.Init(eventManager, entityManager, entityId);
        }

        #endregion
    }
}