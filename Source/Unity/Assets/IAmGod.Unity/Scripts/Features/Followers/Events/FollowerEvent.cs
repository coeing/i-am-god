﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FollowerEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace IAmGod.Unity.Features.Followers.Events
{
    using Slash.ECS.Events;
    
    [GameEventType]
    public enum FollowerEvent
    {
        FollowerAdded,

        FollowerRemoved,
    }
}