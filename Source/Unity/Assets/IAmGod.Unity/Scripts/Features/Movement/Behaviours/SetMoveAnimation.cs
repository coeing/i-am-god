﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetMoveAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Movement.Behaviours
{
    using UnityEngine;

    public class SetMoveAnimation : MonoBehaviour
    {
        #region Fields

        public NavMeshAgent Agent;

        public Animator Animator;

        public string AnimatorKey = "isMoving";

        #endregion

        #region Methods

        protected void Update()
        {
            if (this.Agent != null && this.Animator != null)
            {
                bool isMoving = this.Agent.desiredVelocity.magnitude > 0;
                this.Animator.SetBool(this.AnimatorKey, isMoving);
            }
        }

        #endregion
    }
}