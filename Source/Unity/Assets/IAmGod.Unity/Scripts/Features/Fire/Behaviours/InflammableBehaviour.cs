﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InflammableBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Components;
    using IAmGod.Unity.Features.Fire.Events;

    using Slash.ECS.Events;

    using UnityEngine;

    public class InflammableBehaviour : EntityComponentBehaviour<InflammableComponent>
    {
        #region Fields

        public GameObject fireOnBush;

        public Material InflamedMaterial;

        public Material NormalMaterial;

        public Renderer Renderer;

		Animator anim;

        #endregion



        #region Methods

		// Use this for initialization
		void Start()
		{
			anim = GetComponentInChildren<Animator>();
		}

        protected override void OnInitialized()
        {
            if (this.LogicComponent.BurnState == BurnState.Burning)
            {
                this.Inflame();
            }
            else
            {
                this.Extinguish();
            }
        }

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(FireEvent.Inflamed, this.OnInflamed);
            this.RegisterListener(FireEvent.Extinguished, this.OnExtinguished);
        }

        private void Extinguish()
        {
            if (this.fireOnBush != null)
            {
				//anim.SetBool("isBurning", false);
                this.fireOnBush.SetActive(false);
            }
            else
            {
                this.Renderer.material = this.NormalMaterial;
            }
        }

        private void Inflame()
        {
            if (this.fireOnBush != null)
            {
				//anim.SetBool ("isBurning", true);
                this.fireOnBush.SetActive(true);
            }
            else
            {
                this.Renderer.material = this.InflamedMaterial;
            }
        }

        private void OnExtinguished(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                this.Extinguish();
            }
        }

        private void OnInflamed(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                this.Inflame();
            }
        }

        #endregion
    }
}