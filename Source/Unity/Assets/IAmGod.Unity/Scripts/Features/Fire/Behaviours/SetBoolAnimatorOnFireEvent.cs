﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetBoolAnimatorOnFireEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Events;

    public class SetBoolAnimatorOnFireEvent : SetBoolAnimatorOnEvent<FireEvent>
    {
        #region Methods

        protected override bool CheckEventData(object eventType, object eventData)
        {
            int entityId = (int)eventData;
            return entityId == this.EntityId;
        }

        #endregion
    }
}