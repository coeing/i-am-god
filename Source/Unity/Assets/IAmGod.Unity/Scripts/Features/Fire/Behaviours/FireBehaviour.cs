﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FireBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Components;
    using IAmGod.Unity.Features.Fire.Events;

    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public class FireBehaviour : EntityComponentBehaviour<InflammableComponent>
    {
        #region Methods

        protected void OnCollisionEnter(Collision collision)
        {
            Debug.Log("Collision");
        }

        protected void OnTriggerEnter(Collider other)
        {
            EntityConfigurationBehaviour entityBehaviour = other.GetComponentInParent<EntityConfigurationBehaviour>();
            if (entityBehaviour != null)
            {
                this.Game.EventManager.QueueEvent(
                    FireEvent.FireEntered,
                    new FireEnteredData() { FireEntityId = this.EntityId, EntityId = entityBehaviour.EntityId });
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            EntityConfigurationBehaviour entityBehaviour = other.GetComponentInParent<EntityConfigurationBehaviour>();
            if (entityBehaviour != null)
            {
                this.Game.EventManager.QueueEvent(
                    FireEvent.FireLeft,
                    new FireLeftData() { FireEntityId = this.EntityId, EntityId = entityBehaviour.EntityId });
            }
        }

        #endregion
    }
}