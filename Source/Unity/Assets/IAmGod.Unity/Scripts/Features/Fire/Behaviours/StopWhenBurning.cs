﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StopWhenBurning.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Components;
    using IAmGod.Unity.Features.Fire.Events;

    using Slash.ECS.Events;

    using UnityEngine;

    public class StopWhenBurning : EntityComponentBehaviour<InflammableComponent>
    {
        #region Fields

        public NavMeshAgent Agent;

        public AgentTarget AgentTarget;

        #endregion

        #region Methods

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(FireEvent.Inflamed, this.OnInflamed);
            this.RegisterListener(FireEvent.Extinguished, this.OnExtinguished);
        }

        private void OnExtinguished(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                if (this.Agent.enabled)
                {
                    this.Agent.Resume();
                }
                this.AgentTarget.enabled = true;
            }
        }

        private void OnInflamed(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                if (this.Agent.enabled)
                {
                    this.Agent.Stop();
                }
                this.AgentTarget.enabled = false;
            }
        }

        #endregion
    }
}