﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DestroyOnExtinguish.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Behaviours
{
    using IAmGod.Unity.Core.Behaviours;
    using IAmGod.Unity.Features.Fire.Components;
    using IAmGod.Unity.Features.Fire.Events;

    using Slash.ECS.Events;

    public class DestroyOnExtinguish : EntityComponentBehaviour<InflammableComponent>
    {
        #region Methods

        protected override void OnInitialized()
        {
            if (this.LogicComponent.BurnState != BurnState.Burning)
            {
                this.Extinguish();
            }
        }

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(FireEvent.Extinguished, this.OnExtinguished);
        }

        private void Extinguish()
        {
            Destroy(this.EntityConfigurationBehaviour.gameObject);
        }

        private void OnExtinguished(GameEvent e)
        {
            int entityId = (int)e.EventData;
            if (entityId == this.EntityId)
            {
                this.Extinguish();
            }
        }

        #endregion
    }
}