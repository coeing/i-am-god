﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FireEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace IAmGod.Unity.Features.Fire.Events
{
    using Slash.ECS.Events;

    [GameEventType]
    public enum FireEvent
    {
        Inflamed,

        Extinguished,

        FireEntered,

        FireLeft,
    }
}