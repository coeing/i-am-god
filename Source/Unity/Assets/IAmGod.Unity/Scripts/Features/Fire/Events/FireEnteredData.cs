﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FireEnteredData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Events
{
    public class FireEnteredData
    {
        #region Properties

        /// <summary>
        ///   Id of entity which entered fire.
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        ///   Fire which was entered.
        /// </summary>
        public int FireEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format("FireEntityId: {0}, EntityId: {1}", this.FireEntityId, this.EntityId);
        }

        #endregion
    }
}