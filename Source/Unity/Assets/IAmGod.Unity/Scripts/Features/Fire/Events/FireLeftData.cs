﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FireLeftData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Events
{
    public class FireLeftData
    {
        #region Properties

        /// <summary>
        ///   Id of entity which left fire.
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        ///   Fire which was left.
        /// </summary>
        public int FireEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format("EntityId: {0}, FireEntityId: {1}", this.EntityId, this.FireEntityId);
        }

        #endregion
    }
}