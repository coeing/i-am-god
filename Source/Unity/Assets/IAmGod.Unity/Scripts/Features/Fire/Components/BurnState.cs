﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BurnState.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Components
{
    public enum BurnState
    {
        Off,

        Burning,

        Drowned,
    }
}