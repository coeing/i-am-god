﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InflammableComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Components
{
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class InflammableComponent : EntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Burn State
        /// </summary>
        public const string AttributeBurnState = "InflammableComponent.BurnState";

        /// <summary>
        ///   Attribute: Duration this entity has to be near a burning object to start burning itself (in s).
        /// </summary>
        public const string AttributeIgnitionDuration = "InflammableComponent.IgnitionDuration";

        /// <summary>
        ///   Attribute default: Burn State
        /// </summary>
        public const BurnState DefaultBurnState = BurnState.Off;

        /// <summary>
        ///   Attribute default: Duration this entity has to be near a burning object to start burning itself (in s).
        /// </summary>
        public const float DefaultIgnitionDuration = 0;

        #endregion

        #region Constructors and Destructors

        public InflammableComponent()
        {
            this.BurnState = DefaultBurnState;
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Burn State
        /// </summary>
        [InspectorEnum(AttributeBurnState, Default = DefaultBurnState, Description = "Burn State")]
        public BurnState BurnState { get; set; }

        /// <summary>
        ///   Ignition duration.
        /// </summary>
        [InspectorFloat(AttributeIgnitionDuration, Default = DefaultIgnitionDuration,
            Description = "Duration this entity has to be near a burning object to start burning itself (in s).")]
        public float IgnitionDuration { get; set; }

        /// <summary>
        ///   Duration the entity stood near a burning object (in s).
        /// </summary>
        public float TimeNearFire { get; set; }

        #endregion
    }
}