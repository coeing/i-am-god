﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FireSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Fire.Systems
{
    using System.Collections.Generic;
    using System.Linq;

    using IAmGod.Unity.Features.Fire.Components;
    using IAmGod.Unity.Features.Fire.Events;
    using IAmGod.Unity.Features.Water.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;

    [GameSystem]
    public class FireSystem : GameSystem
    {
        #region Fields

        private readonly List<Inflammable> inflammables = new List<Inflammable>();

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(FireEvent.FireEntered, this.OnFireEntered);
            this.EventManager.RegisterListener(FireEvent.FireLeft, this.OnFireLeft);

            this.EventManager.RegisterListener(WaterEvent.WaterEntered, this.OnWaterEntered);
            this.EventManager.RegisterListener(WaterEvent.WaterExited, this.OnWaterExited);

            this.EntityManager.RegisterComponentListeners<InflammableComponent>(
                this.OnInflammableAdded,
                this.OnInflammableRemoved);
        }

        public override void UpdateSystem(float dt)
        {
            base.UpdateSystem(dt);

            // Check if an entity gets inflamed.
            foreach (var inflammable in this.inflammables)
            {
                // Check if already burning.
                if (inflammable.InflammableComponent.BurnState == BurnState.Burning)
                {
                    continue;
                }

                // Check if drowned, so it can't be inflamed.
                if (inflammable.InflammableComponent.BurnState == BurnState.Drowned)
                {
                    continue;
                }

                // Check if touching any fire.
                if (
                    inflammable.TouchingInflammables.Any(
                        fire => fire.InflammableComponent.BurnState == BurnState.Burning))
                {
                    // Increase time near fire.
                    inflammable.InflammableComponent.TimeNearFire += dt;

                    // Set on fire if ignition duration exceeded.
                    if (inflammable.InflammableComponent.TimeNearFire
                        >= inflammable.InflammableComponent.IgnitionDuration)
                    {
                        this.Inflame(inflammable);
                    }
                }
                else
                {
                    // Reset time near fire.
                    inflammable.InflammableComponent.TimeNearFire = 0;
                }
            }
        }

        #endregion

        #region Methods

        private Inflammable GetInflammableByEntityId(int entityId)
        {
            return this.inflammables.FirstOrDefault(inflammable => inflammable.EntityId == entityId);
        }

        private void Inflame(Inflammable inflammable)
        {
            inflammable.InflammableComponent.BurnState = BurnState.Burning;
            this.EventManager.QueueEvent(FireEvent.Inflamed, inflammable.EntityId);
        }

        private void OnFireEntered(GameEvent e)
        {
            FireEnteredData data = (FireEnteredData)e.EventData;
            this.OnFireEntered(data.FireEntityId, data.EntityId);
        }

        private void OnFireEntered(int fireEntityId, int entityId)
        {
            // Check if inflammable.
            Inflammable inflammable = this.GetInflammableByEntityId(entityId);
            if (inflammable == null)
            {
                // Not inflammable.
                return;
            }

            // Check if inflammable.
            Inflammable fireInflammable = this.GetInflammableByEntityId(fireEntityId);
            if (fireInflammable == null)
            {
                // Not inflammable.
                return;
            }

            // Add to list of fires.
            inflammable.TouchingInflammables.Add(fireInflammable);
        }

        private void OnFireLeft(GameEvent e)
        {
            FireLeftData data = (FireLeftData)e.EventData;
            this.OnFireLeft(data.FireEntityId, data.EntityId);
        }

        private void OnFireLeft(int fireEntityId, int entityId)
        {
            // Check if inflammable.
            Inflammable inflammable = this.GetInflammableByEntityId(entityId);
            if (inflammable == null)
            {
                // Not inflammable.
                return;
            }

            // Check if inflammable.
            Inflammable fireInflammable = this.GetInflammableByEntityId(fireEntityId);
            if (fireInflammable == null)
            {
                // Not inflammable.
                return;
            }

            // Remove from list of fires.
            inflammable.TouchingInflammables.Remove(fireInflammable);
        }

        private void OnInflammableAdded(int entityId, InflammableComponent component)
        {
            this.inflammables.Add(new Inflammable { EntityId = entityId, InflammableComponent = component });
        }

        private void OnInflammableRemoved(int entityId, InflammableComponent component)
        {
            this.inflammables.RemoveAll(inflammable => inflammable.EntityId == entityId);

            // Remove from touching inflammables.
            foreach (var inflammable in this.inflammables)
            {
                inflammable.TouchingInflammables.RemoveAll(
                    touchingInflammable => touchingInflammable.EntityId == entityId);
            }
        }

        private void OnWaterEntered(GameEvent e)
        {
            int entityId = (int)e.EventData;
            Inflammable inflammable = this.GetInflammableByEntityId(entityId);
            if (inflammable == null)
            {
                return;
            }

            bool burned = inflammable.InflammableComponent.BurnState == BurnState.Burning;

            // Set as drowned and reset timer.
            inflammable.InflammableComponent.BurnState = BurnState.Drowned;
            inflammable.InflammableComponent.TimeNearFire = 0;

            if (burned)
            {
                this.EventManager.QueueEvent(FireEvent.Extinguished, inflammable.EntityId);
            }
        }

        private void OnWaterExited(GameEvent e)
        {
            int entityId = (int)e.EventData;
            Inflammable inflammable = this.GetInflammableByEntityId(entityId);
            if (inflammable == null)
            {
                return;
            }

            inflammable.InflammableComponent.BurnState = BurnState.Off;
        }

        #endregion

        public class Inflammable
        {
            #region Constructors and Destructors

            public Inflammable()
            {
                this.TouchingInflammables = new List<Inflammable>();
            }

            #endregion

            #region Properties

            public int EntityId { get; set; }

            public InflammableComponent InflammableComponent { get; set; }

            /// <summary>
            ///   Other inflammables this one is touching.
            /// </summary>
            public List<Inflammable> TouchingInflammables { get; set; }

            #endregion
        }
    }
}