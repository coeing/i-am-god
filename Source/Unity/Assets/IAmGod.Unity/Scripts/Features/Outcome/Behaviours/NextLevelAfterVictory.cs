﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NextLevelAfterVictory.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Features.Outcome.Behaviours
{
    using IAmGod.Unity.Features.Victory.Events;

    using Slash.ECS.Events;
    using Slash.Unity.Common.ECS;
    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class NextLevelAfterVictory : EventManagerOperator
    {
        #region Fields

        public string SceneName;

        public float TimeBeforeNextScene = 3.0f;

        private float timeRemaining;

        private bool victory;

        #endregion

        #region Methods

        protected override void Init()
        {
            base.Init();

            this.SetListener(VictoryEvent.Won, this.OnVictory);
        }

        private void OnVictory(GameEvent e)
        {
            this.victory = true;
            this.timeRemaining = this.TimeBeforeNextScene;
        }

        private void Update()
        {
            if (!this.victory)
            {
                return;
            }

            this.timeRemaining -= Time.deltaTime;

            if (this.timeRemaining > 0)
            {
                return;
            }

            SceneManager.Instance.ChangeScene(this.SceneName);
        }

        #endregion
    }
}