﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OutcomeSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Outcome.Systems
{
    using IAmGod.Unity.Features.Followers.Events;
    using IAmGod.Unity.Features.Health.Events;
    using IAmGod.Unity.Features.Outcome.Components;
    using IAmGod.Unity.Features.Outcome.Events;
    using IAmGod.Unity.Features.Victory.Components;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;

    [GameSystem]
    public class OutcomeSystem : GameSystem
    {
        #region Fields

        private Outcome outcome;

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(FollowerEvent.FollowerAdded, this.OnFollowerAdded);
            this.EventManager.RegisterListener(FollowerEvent.FollowerRemoved, this.OnFollowerRemoved);

            this.EventManager.RegisterListener(HealthEvent.Died, this.OnDied);

            this.EventManager.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
        }

        #endregion

        #region Methods

        private void OnDefeat()
        {
            this.EventManager.QueueEvent(OutcomeEvent.Defeat);
        }

        private void OnDied(GameEvent e)
        {
            int entityId = (int)e.EventData;

            // Check if scorable entity died.
            ScorableComponent scorableComponent = this.EntityManager.GetComponent<ScorableComponent>(entityId);
            if (scorableComponent != null)
            {
                this.OnDefeat();
            }
        }

        private void OnFollowerAdded(GameEvent e)
        {
            ++this.outcome.OutcomeComponent.CurrentFollowers;
            ++this.outcome.OutcomeComponent.MaxFollowers;
            this.EventManager.QueueEvent(OutcomeEvent.Changed, this.outcome.EntityId);
        }

        private void OnFollowerRemoved(GameEvent e)
        {
            --this.outcome.OutcomeComponent.CurrentFollowers;
            this.EventManager.QueueEvent(OutcomeEvent.Changed, this.outcome.EntityId);
            if (this.outcome.OutcomeComponent.CurrentFollowers < 1)
            {
                this.OnDefeat();
            }
        }

        private void OnGameStarted(GameEvent e)
        {
            // Create outcome data.
            this.outcome = new Outcome { EntityId = this.EntityManager.CreateEntity() };
            this.outcome.OutcomeComponent = this.EntityManager.AddComponent<OutcomeComponent>(this.outcome.EntityId);

            this.EventManager.QueueEvent(OutcomeEvent.Initialized, this.outcome.EntityId);
        }

        #endregion

        public class Outcome
        {
            #region Properties

            public int EntityId { get; set; }

            public OutcomeComponent OutcomeComponent { get; set; }

            #endregion
        }
    }
}