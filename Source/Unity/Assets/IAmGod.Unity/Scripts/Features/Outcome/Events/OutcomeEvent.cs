﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OutcomeEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Outcome.Events
{
    using Slash.ECS.Events;

    [GameEventType]
    public enum OutcomeEvent
    {
        /// <summary>
        ///   Outcome system was initialized.
        ///   <para>
        ///     Event data: int (Entity id of outcome result).
        ///   </para>
        /// </summary>
        Initialized,

        Changed,

        Defeat,
    }
}