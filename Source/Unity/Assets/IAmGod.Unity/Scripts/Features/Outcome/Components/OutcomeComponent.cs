﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OutcomeComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Outcome.Components
{
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class OutcomeComponent : EntityComponent
    {
        #region Properties

        /// <summary>
        ///   Current number of followers.
        /// </summary>
        public int CurrentFollowers { get; set; }

        /// <summary>
        ///   Maximum number of followers.
        /// </summary>
        public int MaxFollowers { get; set; }

        #endregion
    }
}