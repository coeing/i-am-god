﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OutcomeContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Outcome.Contexts
{
    using IAmGod.Unity.Core.Contexts;
    using IAmGod.Unity.Features.Outcome.Components;
    using IAmGod.Unity.Features.Outcome.Events;

    using Slash.ECS.Events;
    using Slash.Unity.DataBind.Core.Data;

    public class OutcomeContext : GameContext
    {
        #region Fields

        private readonly Property<int> currentFollowersProperty = new Property<int>();

        private readonly Property<int> maxFollowersProperty = new Property<int>();

        private OutcomeComponent outcomeComponent;

        #endregion

        #region Properties

        public int CurrentFollowers
        {
            get
            {
                return this.currentFollowersProperty.Value;
            }
            set
            {
                this.currentFollowersProperty.Value = value;
            }
        }

        public int MaxFollowers
        {
            get
            {
                return this.maxFollowersProperty.Value;
            }
            set
            {
                this.maxFollowersProperty.Value = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void SetEventListeners()
        {
            base.SetEventListeners();

            this.SetEventListener(OutcomeEvent.Initialized, this.OnOutcomeInitialized);
            this.SetEventListener(OutcomeEvent.Changed, this.OnOutcomeChanged);
        }

        #endregion

        #region Methods

        private void OnOutcomeChanged(GameEvent e)
        {
            int entityId = (int)e.EventData;
            this.outcomeComponent = this.EntityManager.GetComponent<OutcomeComponent>(entityId);
            this.UpdateValues();
        }

        private void OnOutcomeInitialized(GameEvent e)
        {
            int entityId = (int)e.EventData;
            this.outcomeComponent = this.EntityManager.GetComponent<OutcomeComponent>(entityId);
            this.UpdateValues();
        }

        private void UpdateValues()
        {
            if (this.outcomeComponent != null)
            {
                this.CurrentFollowers = this.outcomeComponent.CurrentFollowers;
                this.MaxFollowers = this.outcomeComponent.MaxFollowers;
            }
        }

        #endregion
    }
}