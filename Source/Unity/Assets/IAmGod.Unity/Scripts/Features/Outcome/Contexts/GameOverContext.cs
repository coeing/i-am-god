﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOverContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Features.Outcome.Behaviours
{
    using IAmGod.Unity.Core.Contexts;
    using IAmGod.Unity.Features.Outcome.Events;
    using IAmGod.Unity.Features.Victory.Events;

    using Slash.ECS.Events;
    using Slash.Unity.DataBind.Core.Data;

    public class GameOverContext : GameContext
    {
        #region Fields

        private readonly Property<string> outcomeTextProperty = new Property<string>();

        #endregion

        #region Public Properties

        public string OutcomeText
        {
            get
            {
                return this.outcomeTextProperty.Value;
            }
            set
            {
                this.outcomeTextProperty.Value = value;
            }
        }

        public Property<string> OutcomeTextProperty
        {
            get
            {
                return this.outcomeTextProperty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void SetEventListeners()
        {
            base.SetEventListeners();

            this.SetEventListener(VictoryEvent.Won, this.OnVictory);
            this.SetEventListener(OutcomeEvent.Defeat, this.OnDefeat);
        }

        #endregion

        #region Methods

        private void OnDefeat(GameEvent e)
        {
            this.OutcomeText = "DEFEAT";
        }

        private void OnVictory(GameEvent e)
        {
            this.OutcomeText = "VICTORY";
        }

        #endregion
    }
}