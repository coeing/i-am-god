﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IncreaseScaleOnClick.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Earth
{
    using UnityEngine;

    public class IncreaseScaleOnClick : MonoBehaviour
    {
        #region Fields

        public Vector3 AdditionalScalePerSecond;

        #endregion

        #region Methods

        private void OnMouseOver()
        {
            if (Input.GetMouseButton(0))
            {
                this.gameObject.transform.localScale += this.AdditionalScalePerSecond * Time.deltaTime;
            }
        }

        #endregion
    }
}