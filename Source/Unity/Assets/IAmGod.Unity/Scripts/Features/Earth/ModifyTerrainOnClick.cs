﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModifyTerrainOnClick.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts.Features.Earth
{
    using IAmGod.Unity.Scripts.Input;

    using UnityEngine;

    /// <summary>
    ///   http://answers.unity3d.com/questions/11093/modifying-terrain-height-under-a-gameobject-at-run.html
    /// </summary>
    public class ModifyTerrainOnClick : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Diameter of terrain portion that will raise.
        /// </summary>
        public int Diameter = 50;

        public float ElevateEarthAudioCooldown;

        public AudioClip ElevateEarthClip;

        /// <summary>
        ///   Height to add.
        /// </summary>
        public float Height = 0;

        public Transform NavMeshObstacleParent;

        public float NavMeshObstacleRadiusFactor = 0.5f;

        private float elevateEarthAudioCooldownRemaining;

        private int heightMapHeight;

        private int heightMapWidth;

        private Terrain terrain;

        #endregion

        #region Methods

        private void ElevateTerrainAt(Vector3 worldPosition)
        {
            // Get the normalized position relative to the terrain.
            Vector3 relativePosition = worldPosition - this.terrain.transform.position;
            Vector3 normalizedPosition = new Vector3(
                relativePosition.x / this.terrain.terrainData.size.x,
                relativePosition.y / this.terrain.terrainData.size.y,
                relativePosition.z / this.terrain.terrainData.size.z);

            // Get the position on the terrain heightmap.
            int heightMapPositionX = (int)(normalizedPosition.x * this.heightMapWidth);
            int heightMapPositionY = (int)(normalizedPosition.z * this.heightMapHeight);

            // Get the heights of the terrain to elevate.
            int radius = this.Diameter / 2;
            int radiusSquared = radius * radius;

            float[,] heights = this.terrain.terrainData.GetHeights(
                heightMapPositionX - radius, heightMapPositionY - radius, this.Diameter, this.Diameter);

            // Elevate terrain.
            var height = this.Height * Time.deltaTime;

            for (int i = -radius; i < radius; i++)
            {
                int iSquared = i * i;

                for (int j = -radius; j < radius; j++)
                {
                    int jSquared = j * j;

                    if (iSquared + jSquared < radiusSquared)
                    {
                        heights[i + radius, j + radius] += height;
                    }
                }
            }

            // Set the new heights.
            this.terrain.terrainData.SetHeights(heightMapPositionX - radius, heightMapPositionY - radius, heights);

            // Spawn navigation mesh obstacle.
            if (this.NavMeshObstacleParent == null)
            {
                var parentObject = new GameObject(this.name + "_parent");
                this.NavMeshObstacleParent = parentObject.transform;
            }

            var obstacleObject = new GameObject("Navigation Mesh Obstacle");
            obstacleObject.transform.parent = this.NavMeshObstacleParent;
            obstacleObject.transform.position = worldPosition;

			/* stopping the appearance of navmesh obstacles while crating mountains */ 
            //var obstacle = obstacleObject.AddComponent<NavMeshObstacle>();
            //obstacle.radius = this.Diameter / 2 * this.NavMeshObstacleRadiusFactor;
            //var obstacleCollider = obstacleObject.AddComponent<SphereCollider>();
            //obstacleCollider.isTrigger = true;
            obstacleObject.tag = "NavMeshObstacle";

            if (this.elevateEarthAudioCooldownRemaining <= 0)
            {
                // Play sound.

                var audioSource = obstacleObject.AddComponent<AudioSource>();
                audioSource.clip = this.ElevateEarthClip;
                audioSource.playOnAwake = false;
                audioSource.volume = .05f;
                audioSource.Play();

                // Set cooldown.
                this.elevateEarthAudioCooldownRemaining = this.ElevateEarthAudioCooldown;
            }
        }

        private void LowerTerrainAt(Vector3 worldPosition)
        {
            // Get the normalized position relative to the terrain.
            Vector3 relativePosition = worldPosition - this.terrain.transform.position;
            Vector3 normalizedPosition = new Vector3(
                relativePosition.x / this.terrain.terrainData.size.x,
                relativePosition.y / this.terrain.terrainData.size.y,
                relativePosition.z / this.terrain.terrainData.size.z);

            // Get the position on the terrain heightmap.
            int heightMapPositionX = (int)(normalizedPosition.x * this.heightMapWidth);
            int heightMapPositionY = (int)(normalizedPosition.z * this.heightMapHeight);

            // Get the heights of the terrain to elevate.
            int radius = this.Diameter / 2;
            int radiusSquared = radius * radius;

            float[,] heights = this.terrain.terrainData.GetHeights(
                heightMapPositionX - radius, heightMapPositionY - radius, this.Diameter, this.Diameter);

            // Elevate terrain.
            var height = this.Height * Time.deltaTime * -1;

            for (int i = -radius; i < radius; i++)
            {
                int iSquared = i * i;

                for (int j = -radius; j < radius; j++)
                {
                    int jSquared = j * j;

                    if (iSquared + jSquared < radiusSquared)
                    {
                        heights[i + radius, j + radius] += height;

                        //						// DEBUG
                        //						int aux1 = heightMapPositionX - radius;
                        //						int aux2 = heightMapPositionY - radius;
                        //						//int aux3 = heights;
                        //						Debug.Log("X: " + aux1.ToString() + " Y: " + aux2.ToString() +
                        //						          " Z: " + heights[i+radius, j+radius].ToString());
                    }
                }
            }

            // Set the new heights.
            this.terrain.terrainData.SetHeights(heightMapPositionX - radius, heightMapPositionY - radius, heights);

            /* ABANDONED THE MESH OBSTACLE DESTROYER. TOO MUCH */

            //			// Spawn navigation mesh obstacle destroyer.
            //			var obstacleObjectDestroyer = new GameObject("Navigation Mesh Obstacle Destroyer");
            //			obstacleObjectDestroyer.transform.parent = this.transform;
            //			obstacleObjectDestroyer.transform.position = worldPosition;
            //			
            //			//var obstacle = obstacleObjectDestroyer.AddComponent<NavMeshObstacle>();
            //			//obstacle.radius = this.Diameter / 2 * this.NavMeshObstacleRadiusFactor;
            //			var obstacleCollider = obstacleObjectDestroyer.AddComponent<SphereCollider>();
            //			obstacleCollider.isTrigger = true;
            //			obstacleCollider.radius = 5f;
            //
            //			var obstacleScript = obstacleObjectDestroyer.AddComponent<DoOnTriggerEnter>();

            //obstacleObjectDestroyer.tag = "NavMeshObstacle";
        }

        private void OnEnable()
        {
            this.audio.Play();
        }

        private void Start()
        {
            this.terrain = Terrain.activeTerrain;
            this.heightMapWidth = this.terrain.terrainData.heightmapWidth;
            this.heightMapHeight = this.terrain.terrainData.heightmapHeight;
        }

        private void Update()
        {
            if (InputManager.InputProvider.IsButtonPressed(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(InputManager.InputProvider.PointerPosition);
                if (Physics.Raycast(ray, out hit))
                {
                    this.ElevateTerrainAt(hit.point);
                }
            } 
                // Lower the terrain when B button or the left mouse click
            else if (InputManager.InputProvider.IsButtonPressed(5))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(InputManager.InputProvider.PointerPosition);
                if (Physics.Raycast(ray, out hit))
                {
                    this.LowerTerrainAt(hit.point);
                }
            }

            if (this.elevateEarthAudioCooldownRemaining > 0)
            {
                this.elevateEarthAudioCooldownRemaining -= Time.deltaTime;
            }
        }

        #endregion
    }
}