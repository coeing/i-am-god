﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaterBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Features.Water.Behaviours
{
    using IAmGod.Unity.Features.Water.Events;

    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public class WaterBehaviour : GameEventBehaviour
    {
        #region Methods

        protected void OnTriggerEnter(Collider other)
        {
            EntityConfigurationBehaviour entityBehaviour = other.GetComponentInParent<EntityConfigurationBehaviour>();
            if (entityBehaviour != null)
            {
                this.Game.EventManager.QueueEvent(WaterEvent.WaterEntered, entityBehaviour.EntityId);
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            EntityConfigurationBehaviour entityBehaviour = other.GetComponentInParent<EntityConfigurationBehaviour>();
            if (entityBehaviour != null)
            {
                this.Game.EventManager.QueueEvent(WaterEvent.WaterExited, entityBehaviour.EntityId);
            }
        }

        #endregion
    }
}