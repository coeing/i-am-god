﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaterEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace IAmGod.Unity.Features.Water.Events
{
    using Slash.ECS.Events;

    [GameEventType]
    public enum WaterEvent
    {
        WaterEntered,

        WaterExited,
    }
}