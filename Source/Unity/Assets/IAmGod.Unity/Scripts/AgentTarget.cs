﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AgentTarget.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity
{
    using UnityEngine;

    public class AgentTarget : MonoBehaviour
    {
        #region Fields

        public NavMeshAgent Agent;

        public Transform Target;

        #endregion

        #region Methods

        private void Update()
        {
            if (this.Agent != null && this.Agent.enabled && this.Target != null)
            {
                this.Agent.SetDestination(this.Target.position);
            }
        }

        #endregion
    }
}