﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DestroyedOnCollision.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace IAmGod.Unity.Scripts
{
    using UnityEngine;

    public class DestroyOnCollision : MonoBehaviour
    {
        #region Fields

        public string LethalGameObjectTag;

        #endregion

        #region Methods

        private void OnTriggerEnter(Collider col)
        {
            if (col.gameObject.tag.Equals(this.LethalGameObjectTag))
            {
                Destroy(this.gameObject);
            }
        }

        #endregion
    }
}